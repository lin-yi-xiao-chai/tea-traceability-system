package com.powernode.teatraceability;


import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.StringJoiner;

@Slf4j
public class Test {


    @org.junit.Test
    public void getValues(){
        String s = "qydm,name,money,product,number,time,picture,create_time";
        String[] ans = s.split(",");
        final StringBuilder builder = new StringBuilder();
        for(int i=0;i<ans.length;++i) {
            StringJoiner joiner = new StringJoiner("", "#{", "}");
            joiner.add(ans[i]);
            builder.append(joiner.toString());
            if(i!= ans.length-1) builder.append(",");
        }
        System.out.println(builder.toString());
    }

    @org.junit.Test
    public void testString(){
        String a = "123";
    }

    @org.junit.Test
    public void testRandom(){
        String s;
        do {
            s = String.valueOf((int) (Math.random() * (int) 1e8));
        } while(s.length()==8);
    }

    @org.junit.Test
    public void getNowTime() throws ParseException {
        Date date = new Date();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        String format = simpleDateFormat.format(date);
        System.out.println(format);
        Date parse = simpleDateFormat.parse(format);
        System.out.println(parse);
    }

    @org.junit.jupiter.api.Test
    public void getSym(){
        String cpbh = "20387";
        StringBuilder builder = new StringBuilder();
        StringBuilder temp = new StringBuilder();
        for(int i=0;i<=9;++i) temp.append(i);
        for(int i=0;i<26;++i) temp.append((char) ('A'+i));
        builder.append(1100);
        int bh = Integer.parseInt(cpbh);
        for(int i=0;i<4;++i){
            int x = bh%10;
            bh /= 10;
            builder.append((i==0||i==2)?(temp.charAt(x+10)):(temp.charAt(x)));
        }
        log.info("sym->{}",builder.toString());
    }

}
