package com.powernode.teatraceability;

import com.powernode.teatraceability.dao.sourceinfo.OriginFileMapper;
import com.powernode.teatraceability.dao.sourceinfo.ProductFileMapper;
import com.powernode.teatraceability.pojo.sourceinfo.OriginFile;
import com.powernode.teatraceability.pojo.sourceinfo.ProductFile;
import com.powernode.teatraceability.service.basicinfomgt.EnterpriseService;
import com.powernode.teatraceability.service.user.UserService;
import com.powernode.teatraceability.aspects.EnterpriseAspect;
import com.powernode.teatraceability.util.GlobalConfig;
import com.powernode.teatraceability.util.JsonData;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;
import java.util.Arrays;

@SpringBootTest
@Slf4j
class TeaTraceabilityApplicationTests {

    @Resource
    private EnterpriseService enterpriseService;
    @Resource
    private UserService userService;

    @Resource
    private OriginFileMapper originFileMapper;
    @Resource
    private ProductFileMapper productFileMapper;

    @Test
    public void testProductFile(){
        ProductFile productFile = productFileMapper.selectOne("21051101");
        log.info("product->{}",productFile.getProduct());
        log.info("detection->{}",productFile.getDetectionInfo());
        log.info("enterprise->{}",productFile.getEnterprise());
    }

    @Test
    public void testOriginFile(){
        OriginFile originFile = originFileMapper.selectOne("13687");
        System.out.println(originFile);
    }

    @Test
    void contextLoads() {
    }

    @Test
    public void testUserService(){
        boolean flag = userService.verify("hnkj","123456","系统管理员");
        if(flag){
            System.out.println("登陆成功，可在此位置建立会话");
        }else{
            System.out.println("登陆失败，再次跳转到登陆页面，重新登陆");
        }
    }

    /*@Test
    public void testEnterpriseService(){
        Enterprise enterprise = enterpriseService.queryByQydm("340104003001");
        System.out.println(enterprise);
        Double random = 117 + Math.random();
        System.out.println(random.toString().substring(0,17));
    }*/
    @Resource
    EnterpriseAspect enterpriseAspect;
    @Test
    public void testThreadLocal(){
        JsonData jsonData = GlobalConfig.LOCAL.get();
        System.out.println(jsonData);
    }

    @Test
    public void getAns(){
        String s = "qydm,trpmc,djzh,jsr,syqx,sydx,syrq,bz,gg,sysl,syff,scs,fzxx,nyzj";
        String[] ans = s.split(",");
        System.out.println(Arrays.toString(ans));
    }


}
