package com.powernode.teatraceability.util;


import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class JsonData<T>{

    private int status;

    private String msg;

    private T data;

}
