package com.powernode.teatraceability.util;

import com.powernode.teatraceability.pojo.login.User;
import com.powernode.teatraceability.pojo.page.PageRequest;
import com.powernode.teatraceability.pojo.page.PageResult;

public class GlobalConfig {

    private static User user;
    public final static ThreadLocal<JsonData> LOCAL = ThreadLocal.withInitial(JsonData::new);
    public final static ThreadLocal<PageRequest> PAGE_REQUEST = ThreadLocal.withInitial(PageRequest::new);

    public final static ThreadLocal<PageResult> PAGE_RESULT = ThreadLocal.withInitial(PageResult::new);

    private GlobalConfig(){}

    public static User getUser() {
        return user;
    }

    public static void setUser(User user) {
        GlobalConfig.user = user;
    }
    public static void initPageRequest(int pageNum,int pageSize){
        PageRequest pageRequest = PAGE_REQUEST.get();
        pageRequest.setPageSize(pageSize);
        pageRequest.setPageNum(pageNum);
    }

    public static void setEmptyJsonData(){
        JsonData jsonData = LOCAL.get();
        jsonData.setMsg(null);
        jsonData.setStatus(0);
        jsonData.setData(null);
    }
    public static void setEmptyPageRequest(){
        PageRequest pageRequest = PAGE_REQUEST.get();
        pageRequest.setPageNum(0);
        pageRequest.setPageSize(0);
    }
}
