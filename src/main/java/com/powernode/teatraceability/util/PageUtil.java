package com.powernode.teatraceability.util;

import com.github.pagehelper.PageInfo;
import com.powernode.teatraceability.pojo.page.PageRequest;
import com.powernode.teatraceability.pojo.page.PageResult;
import net.sf.jsqlparser.statement.select.Top;

/**
 * 分页工具类
 */
public class PageUtil {

    private PageUtil(){}

    public static PageResult getPageResult(PageInfo<?> pageInfo){
        PageResult pageResult = GlobalConfig.PAGE_RESULT.get();
        pageResult.setPageNum(pageInfo.getPageNum());
        pageResult.setPageSize(pageInfo.getPageSize());
        pageResult.setInfo(pageInfo.getList());
        pageResult.setTotalNum(pageInfo.getPages());
        pageResult.setTotalSize(pageInfo.getSize());
        pageResult.setTotal(pageInfo.getTotal());
        // System.out.println(total);
        return pageResult;
    }

}
