package com.powernode.teatraceability.controller.plateprocessmgt;


import com.powernode.teatraceability.dao.plateprocessmgt.DetectionInfoMapper;
import com.powernode.teatraceability.pojo.login.User;
import com.powernode.teatraceability.pojo.plateprocessmgt.DetectionInfo;
import com.powernode.teatraceability.service.plateprocessmgt.DetectionInfoService;
import com.powernode.teatraceability.util.GlobalConfig;
import com.powernode.teatraceability.util.JsonData;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

@RestController
@RequestMapping("/detection/info")
public class DetectionInfoController {

    @Resource
    private DetectionInfoService detectionInfoService;


    @GetMapping("/{cpmc}/{pageNum}/{pageSize}")
    public JsonData selectDetectionInfo(@PathVariable int pageNum,
                                        @PathVariable int pageSize,
                                        @SessionAttribute User user,
                                        @PathVariable String cpmc){
        GlobalConfig.initPageRequest(pageNum,pageSize);
        detectionInfoService.selectLike(user.getQydm(), cpmc);
        return GlobalConfig.LOCAL.get();
    }

    @GetMapping("/{ybmc}")
    public List<String> selectAllPch(@PathVariable String ybmc,
                                     @SessionAttribute User user){
        return detectionInfoService.queryAllPch(ybmc,user.getQydm());
    }

    @GetMapping("/{pageNum}/{pageSize}")
    public JsonData selectDetectionInfo(@PathVariable int pageNum,
                                        @PathVariable int pageSize,
                                        @SessionAttribute User user){
        GlobalConfig.initPageRequest(pageNum,pageSize);
        detectionInfoService.selectAllByQydm(user.getQydm());
        return GlobalConfig.LOCAL.get();
    }

    @PostMapping
    public JsonData insert(@RequestBody DetectionInfo detectionInfo,
                           @SessionAttribute User user){
        detectionInfo.setQydm(user.getQydm());
        detectionInfoService.insert(detectionInfo);
        return GlobalConfig.LOCAL.get();
    }

    @PutMapping
    public JsonData update(@RequestBody DetectionInfo detectionInfo){
        detectionInfoService.update(detectionInfo);
        return GlobalConfig.LOCAL.get();
    }

    @DeleteMapping("/{bh}")
    public JsonData deleteByBh(@PathVariable int bh){
        detectionInfoService.delete(bh);
        return GlobalConfig.LOCAL.get();
    }

}
