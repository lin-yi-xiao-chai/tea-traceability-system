package com.powernode.teatraceability.controller.plateprocessmgt;


import com.powernode.teatraceability.service.plateprocessmgt.InputService;
import com.powernode.teatraceability.util.GlobalConfig;
import com.powernode.teatraceability.util.JsonData;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
@RequestMapping("/input")
public class InputController {

    @Resource
    private InputService inputService;

    @GetMapping("/{nydjzh}")
    public JsonData selectByDjzh(@PathVariable String nydjzh){
        inputService.queryByDjzh(nydjzh);
        return GlobalConfig.LOCAL.get();
    }

}
