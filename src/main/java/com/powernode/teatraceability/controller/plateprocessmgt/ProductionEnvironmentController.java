package com.powernode.teatraceability.controller.plateprocessmgt;

import com.powernode.teatraceability.pojo.login.User;
import com.powernode.teatraceability.service.plateprocessmgt.ProductionEnvironmentService;
import com.powernode.teatraceability.util.GlobalConfig;
import com.powernode.teatraceability.util.JsonData;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

@RestController
@RequestMapping("/production/environment")
public class ProductionEnvironmentController {

    @Resource
    private ProductionEnvironmentService productionEnvironmentService;

    @GetMapping("/{pageNum}/{pageSize}")
    public JsonData selectProductionEnvironmentInfo(@PathVariable int pageNum,
                                                    @PathVariable int pageSize,
                                                    @SessionAttribute User user){
        GlobalConfig.initPageRequest(pageNum,pageSize);
        productionEnvironmentService.selectAll(user.getQydm());
        return GlobalConfig.LOCAL.get();
    }

}
