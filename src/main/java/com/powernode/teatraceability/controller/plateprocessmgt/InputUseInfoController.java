package com.powernode.teatraceability.controller.plateprocessmgt;


import com.powernode.teatraceability.dao.basicinfomgt.EnterpriseMapper;
import com.powernode.teatraceability.pojo.login.User;
import com.powernode.teatraceability.pojo.plateprocessmgt.InputUseInfo;
import com.powernode.teatraceability.service.plateprocessmgt.InputUseInfoService;
import com.powernode.teatraceability.util.GlobalConfig;
import com.powernode.teatraceability.util.JsonData;
import org.apache.ibatis.annotations.Param;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.xml.namespace.QName;

@RestController
@RequestMapping("/input/use")
public class InputUseInfoController {
    @Resource
    private InputUseInfoService inputUseInfoService;

    @GetMapping("/{trpmc}/{pageNum}/{pageSize}")
    public JsonData selectLikeByTrpmc(@PathVariable int pageNum,
                                      @PathVariable int pageSize,
                                      @PathVariable String trpmc,
                                      @SessionAttribute User user){
        GlobalConfig.initPageRequest(pageNum,pageSize);
        inputUseInfoService.selectLikeByTrpmc(user.getQydm(),trpmc);
        return GlobalConfig.LOCAL.get();
    }

    @GetMapping("/{pageNum}/{pageSize}")
    public JsonData selectPageByQydm(@SessionAttribute User user,
                                       @PathVariable int pageNum,
                                       @PathVariable int pageSize){
        GlobalConfig.initPageRequest(pageNum,pageSize);
        inputUseInfoService.selectByQydm(user.getQydm());
        return GlobalConfig.LOCAL.get();
    }

    @PostMapping
    public JsonData insert(@RequestBody InputUseInfo useInfo){
        inputUseInfoService.insert(useInfo);
        return GlobalConfig.LOCAL.get();
    }

    @PutMapping
    public JsonData update(@RequestBody InputUseInfo useInfo){
        inputUseInfoService.update(useInfo);
        return GlobalConfig.LOCAL.get();
    }

    @DeleteMapping("/{bh}")
    public JsonData delete(@PathVariable int bh){
        inputUseInfoService.delete(bh);
        return GlobalConfig.LOCAL.get();
    }

}
