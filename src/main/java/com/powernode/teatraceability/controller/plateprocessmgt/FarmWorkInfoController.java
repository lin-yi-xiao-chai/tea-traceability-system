package com.powernode.teatraceability.controller.plateprocessmgt;


import com.powernode.teatraceability.pojo.login.User;
import com.powernode.teatraceability.pojo.plateprocessmgt.FarmWorkInfo;
import com.powernode.teatraceability.service.plateprocessmgt.FarmWorkInfoService;
import com.powernode.teatraceability.util.GlobalConfig;
import com.powernode.teatraceability.util.JsonData;
import jdk.nashorn.internal.objects.Global;
import org.apache.ibatis.annotations.Delete;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

@RestController
@RequestMapping("/farm/work")
public class FarmWorkInfoController {

    @Resource
    private FarmWorkInfoService farmWorkInfoService;

    @GetMapping("/like/{nszy}/{pageNum}/{pageSize}")
    public JsonData selectLike(@PathVariable String nszy,
                                   @PathVariable int pageNum,
                                   @PathVariable int pageSize,
                                   @SessionAttribute User user){
        GlobalConfig.initPageRequest(pageNum, pageSize);
        farmWorkInfoService.selectLike(user.getQydm(), nszy);
        return GlobalConfig.LOCAL.get();
    }

    @GetMapping("/{zyr}/{pageNum}/{pageSize}")
    public JsonData selectAllByZyr(@PathVariable String zyr,
                                   @PathVariable int pageNum,
                                   @PathVariable int pageSize,
                                   @SessionAttribute User user){
        GlobalConfig.initPageRequest(pageNum,pageSize);
        farmWorkInfoService.selectAllByZyr(zyr,user.getQydm());
        return GlobalConfig.LOCAL.get();
    }

    @GetMapping("/{pageNum}/{pageSize}")
    public JsonData selectAllByQydm(@SessionAttribute User user,
                                    @PathVariable int pageNum,
                                    @PathVariable int pageSize){
        GlobalConfig.initPageRequest(pageNum,pageSize);
        farmWorkInfoService.selectAllByQydm(user.getQydm());
        return GlobalConfig.LOCAL.get();
    }

    @PostMapping
    public JsonData insert(@RequestBody FarmWorkInfo farmWorkInfo,
                           @SessionAttribute User user){
        farmWorkInfo.setQydm(user.getQydm());
        farmWorkInfoService.insert(farmWorkInfo);
        return GlobalConfig.LOCAL.get();
    }

    @PutMapping
    public JsonData update(@RequestBody FarmWorkInfo farmWorkInfo){
        farmWorkInfoService.update(farmWorkInfo);
        return GlobalConfig.LOCAL.get();
    }

    @DeleteMapping("/{bh}")
    public JsonData delete(@PathVariable int bh){
        farmWorkInfoService.delete(bh);
        return GlobalConfig.LOCAL.get();
    }

}
