package com.powernode.teatraceability.controller.plateprocessmgt;


import com.powernode.teatraceability.pojo.login.User;
import com.powernode.teatraceability.pojo.page.PageResult;
import com.powernode.teatraceability.pojo.plateprocessmgt.InputPurchaseInfo;
import com.powernode.teatraceability.service.plateprocessmgt.InputPurchaseInfoService;
import com.powernode.teatraceability.util.GlobalConfig;
import com.powernode.teatraceability.util.JsonData;
import jdk.nashorn.internal.objects.Global;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.tags.form.InputTag;

import javax.annotation.Resource;

@RestController
@RequestMapping("/input/purchase")
public class InputPurchaseInfoController {

    @Resource
    private InputPurchaseInfoService inputPurchaseInfoService;
    
    @GetMapping("/{trpmc}/{pageNum}/{pageSize}")
    public JsonData selectLike(@PathVariable int pageNum,
                               @PathVariable int pageSize,
                               @SessionAttribute User user,
                               @PathVariable String trpmc){
        GlobalConfig.initPageRequest(pageNum, pageSize);
        inputPurchaseInfoService.selectLike(user.getQydm(),trpmc);
        return GlobalConfig.LOCAL.get();
    }

    @GetMapping("/{pageNum}/{pageSize}")
    public JsonData selectAllByQydm(@PathVariable int pageNum,
                                      @PathVariable int pageSize,
                                      @SessionAttribute User user){
        GlobalConfig.initPageRequest(pageNum,pageSize);
        inputPurchaseInfoService.selectAllByQydm(user.getQydm());
        // 返回分页结果
        return GlobalConfig.LOCAL.get();
    }

    @DeleteMapping("/{bh}")
    public JsonData delete(@PathVariable int bh){
        inputPurchaseInfoService.delete(bh);
        return GlobalConfig.LOCAL.get();
    }

    @PutMapping
    public JsonData update(@RequestBody InputPurchaseInfo inputPurchaseInfo){
        inputPurchaseInfoService.update(inputPurchaseInfo);
        return GlobalConfig.LOCAL.get();
    }

    @PostMapping
    public JsonData insert(@RequestBody InputPurchaseInfo inputPurchaseInfo,
                           @SessionAttribute User user){
        inputPurchaseInfo.setQydm(user.getQydm());
        inputPurchaseInfoService.insert(inputPurchaseInfo);
        return GlobalConfig.LOCAL.get();
    }

}
