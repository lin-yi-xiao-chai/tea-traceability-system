package com.powernode.teatraceability.controller.sourceinfo;


import com.powernode.teatraceability.dao.plateprocessmgt.ProductBatchMapper;
import com.powernode.teatraceability.pojo.login.User;
import com.powernode.teatraceability.pojo.sourceinfo.SourceCode;
import com.powernode.teatraceability.service.basicinfomgt.OriginService;
import com.powernode.teatraceability.service.basicinfomgt.ProductService;
import com.powernode.teatraceability.service.plateprocessmgt.ProductBatchService;
import com.powernode.teatraceability.service.sourceinfo.RecordService;
import com.powernode.teatraceability.service.sourceinfo.SourceCodeService;
import com.powernode.teatraceability.util.GlobalConfig;
import com.powernode.teatraceability.util.JsonData;
import org.apache.ibatis.annotations.Param;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

@RestController
@RequestMapping("/source")
public class SourceCodeController {

    @Resource
    private SourceCodeService sourceCodeService;
    @Resource
    private ProductBatchService productBatchService;
    @Resource
    private RecordService recordService;

    @GetMapping("/{pch}")
    public JsonData getRecordInfo(@PathVariable String pch){
        return recordService.getRecordInfo(pch);
    }

    @GetMapping("/{pageNum}/{pageSize}")
    public JsonData selectAll(@PathVariable int pageNum,
                              @PathVariable int pageSize,
                              @SessionAttribute User user){
        GlobalConfig.initPageRequest(pageNum, pageSize);
        sourceCodeService.selectAll(user.getQydm());
        return GlobalConfig.LOCAL.get();
    }

    @PutMapping("/{bh}/{smcs}")
    public JsonData update(@PathVariable int bh,
                           @PathVariable int smcs){
        sourceCodeService.updateSmcs(bh, smcs);
        return GlobalConfig.LOCAL.get();
    }

    @PostMapping
    public JsonData insert(@RequestBody SourceCode sourceCode,
                           @SessionAttribute User user) throws ParseException {
        sourceCode.setQydm(user.getQydm());
        sourceCode.setCdbh(productBatchService.queryCdbhByPch(sourceCode.getPch()));
        sourceCode.setCpbh(productBatchService.queryCpbhByPch(sourceCode.getPch()));
        sourceCode.setDate(getNowTime());
        sourceCode.setSym(getSym(sourceCode.getCpbh()));
        sourceCode.setSmcs(0);
        sourceCode.setType(0);
        sourceCodeService.iinsert(sourceCode);
        return GlobalConfig.LOCAL.get();
    }

    private String getSym(String cpbh){
        StringBuilder builder = new StringBuilder();
        StringBuilder temp = new StringBuilder();
        for(int i=0;i<=9;++i) temp.append(i);
        for(int i=0;i<26;++i) temp.append((char) ('A'+i));
        builder.append(1100);
        int bh = Integer.parseInt(cpbh);
        for(int i=0;i<4;++i){
            int x = bh%10;
            bh /= 10;
            builder.append((i==0||i==2)?(temp.charAt(x+10)):(temp.charAt(x)));
        }
        builder.append("011");
        builder.append((int)(Math.random()*10000));
        return builder.toString();
    }

    private Date getNowTime() throws ParseException {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        String format = simpleDateFormat.format(new Date());
        return simpleDateFormat.parse(format);
    }

}
