package com.powernode.teatraceability.controller.processmgt;


import com.powernode.teatraceability.pojo.login.User;
import com.powernode.teatraceability.pojo.processmgt.OutputStorageMgt;
import com.powernode.teatraceability.service.processmgt.OutputStorageMgtService;
import com.powernode.teatraceability.util.GlobalConfig;
import com.powernode.teatraceability.util.JsonData;
import jdk.nashorn.internal.objects.Global;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

@RestController
@RequestMapping("/storage/output")
public class OutputStorageMgtController {

    @Resource
    private OutputStorageMgtService outputStorageMgtService;

    @GetMapping("/{cpmc}/{pageNum}/{pageSize}")
    public JsonData selectLike(@PathVariable int pageNum,
                              @PathVariable int pageSize,
                              @PathVariable String cpmc,
                              @SessionAttribute User user){
        GlobalConfig.initPageRequest(pageNum, pageSize);
        outputStorageMgtService.selectLike(user.getQydm(),cpmc);
        return GlobalConfig.LOCAL.get();
    }

    @GetMapping("/{pageNum}/{pageSize}")
    public JsonData selectAll(@PathVariable int pageNum,
                              @PathVariable int pageSize,
                              @SessionAttribute User user){
        GlobalConfig.initPageRequest(pageNum, pageSize);
        outputStorageMgtService.selectAll(user.getQydm());
        return GlobalConfig.LOCAL.get();
    }

    @DeleteMapping("/{id}")
    public JsonData delete(@PathVariable int id){
        outputStorageMgtService.delete(id);
        return GlobalConfig.LOCAL.get();
    }

    @PutMapping
    public JsonData update(@RequestBody OutputStorageMgt outputStorageMgt){
        outputStorageMgtService.update(outputStorageMgt);
        return GlobalConfig.LOCAL.get();
    }

    @PostMapping
    public JsonData insert(@RequestBody OutputStorageMgt outputStorageMgt,
                           @SessionAttribute User user){
        outputStorageMgt.setQydm(user.getQydm());
        outputStorageMgtService.insert(outputStorageMgt);
        return GlobalConfig.LOCAL.get();
    }

}
