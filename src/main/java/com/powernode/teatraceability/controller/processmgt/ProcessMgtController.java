package com.powernode.teatraceability.controller.processmgt;


import com.powernode.teatraceability.pojo.login.User;
import com.powernode.teatraceability.pojo.processmgt.ProcessMgt;
import com.powernode.teatraceability.service.processmgt.ProcessMgtService;
import com.powernode.teatraceability.util.GlobalConfig;
import com.powernode.teatraceability.util.JsonData;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

@RestController
@RequestMapping("/process/mgt")
public class ProcessMgtController {

    @Resource
    private ProcessMgtService processMgtService;

    @GetMapping("/{fzr}/{pageNum}/{pageSize}")
    public JsonData selectLike(@PathVariable int pageNum,
                              @PathVariable int pageSize,
                              @PathVariable String fzr,
                              @SessionAttribute User user){
        GlobalConfig.initPageRequest(pageNum, pageSize);
        processMgtService.selectLike(user.getQydm(), fzr);
        return GlobalConfig.LOCAL.get();
    }

    @GetMapping("/{pageNum}/{pageSize}")
    public JsonData selectAll(@PathVariable int pageNum,
                              @PathVariable int pageSize,
                              @SessionAttribute User user){
        GlobalConfig.initPageRequest(pageNum,pageSize);
        processMgtService.selectAll(user.getQydm());
        return GlobalConfig.LOCAL.get();
    }

    @DeleteMapping("/{id}")
    public JsonData deleteById(@PathVariable int id){
        processMgtService.deleteById(id);
        return GlobalConfig.LOCAL.get();
    }

    @PostMapping
    public JsonData insert(@RequestBody ProcessMgt processMgt,
                           @SessionAttribute User user){
        processMgt.setQydm(user.getQydm());
        processMgtService.insert(processMgt);
        return GlobalConfig.LOCAL.get();
    }
    @PutMapping
    public JsonData update(@RequestBody ProcessMgt processMgt){
        processMgtService.update(processMgt);
        return GlobalConfig.LOCAL.get();
    }
}
