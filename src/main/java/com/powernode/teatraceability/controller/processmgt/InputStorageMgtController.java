package com.powernode.teatraceability.controller.processmgt;


import com.powernode.teatraceability.pojo.login.User;
import com.powernode.teatraceability.pojo.processmgt.InputStorageMgt;
import com.powernode.teatraceability.service.processmgt.InputStorageMgtService;
import com.powernode.teatraceability.util.GlobalConfig;
import com.powernode.teatraceability.util.JsonData;
import jdk.nashorn.internal.objects.Global;
import org.apache.ibatis.annotations.Delete;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

@RestController
@RequestMapping("/storage/input")
public class InputStorageMgtController {

    @Resource
    private InputStorageMgtService inputStorageMgtService;

    @GetMapping("/{supplyName}/{pageNum}/{pageSize}")
    public JsonData selectLike(@PathVariable int pageNum,
                               @PathVariable int pageSize,
                               @PathVariable String supplyName,
                               @SessionAttribute User user){
        GlobalConfig.initPageRequest(pageNum, pageSize);
        inputStorageMgtService.selectLike(user.getQydm(), supplyName);
        return GlobalConfig.LOCAL.get();
    }

    @GetMapping("/{pageNum}/{pageSize}")
    public JsonData selectAll(@PathVariable int pageNum,
                              @PathVariable int pageSize,
                              @SessionAttribute User user){
        GlobalConfig.initPageRequest(pageNum,pageSize);
        inputStorageMgtService.selectAll(user.getQydm());
        return GlobalConfig.LOCAL.get();
    }

    @DeleteMapping("/{id}")
    public JsonData delete(@PathVariable int id){
        inputStorageMgtService.delete(id);
        return GlobalConfig.LOCAL.get();
    }

    @PutMapping
    public JsonData update(@RequestBody InputStorageMgt inputStorageMgt){
        inputStorageMgtService.update(inputStorageMgt);
        return GlobalConfig.LOCAL.get();
    }

    @PostMapping
    public JsonData insert(@RequestBody InputStorageMgt inputStorageMgt,
                           @SessionAttribute User user){
        inputStorageMgt.setQydm(user.getQydm());
        inputStorageMgtService.insert(inputStorageMgt);
        return GlobalConfig.LOCAL.get();
    }

}
