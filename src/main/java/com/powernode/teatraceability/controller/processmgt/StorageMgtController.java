package com.powernode.teatraceability.controller.processmgt;


import com.powernode.teatraceability.pojo.processmgt.StorageMgt;
import com.powernode.teatraceability.service.processmgt.StorageMgtService;
import com.powernode.teatraceability.util.GlobalConfig;
import com.powernode.teatraceability.util.JsonData;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

@RestController
@RequestMapping("/storage/mgt")
public class StorageMgtController {
    @Resource
    private StorageMgtService storageMgtService;

    @GetMapping
    public JsonData selectAllStorage(){
        storageMgtService.queryAllStorage();
        return GlobalConfig.LOCAL.get();
    }

    @GetMapping("/{storageName}/{pageNum}/{pageSize}")
    public JsonData selectLike(@PathVariable int pageSize,
                               @PathVariable int pageNum,
                               @PathVariable String storageName){
        GlobalConfig.initPageRequest(pageNum, pageSize);
        storageMgtService.selectLike(storageName);
        return GlobalConfig.LOCAL.get();
    }

    @GetMapping("/{pageNum}/{pageSize}")
    public JsonData selectAll(@PathVariable int pageNum,
                              @PathVariable int pageSize){
        GlobalConfig.initPageRequest(pageNum,pageSize);
        storageMgtService.selectAll();
        return GlobalConfig.LOCAL.get();
    }

    @DeleteMapping("/{id}")
    public JsonData delete(@PathVariable int id){
        storageMgtService.delete(id);
        return GlobalConfig.LOCAL.get();
    }

    @PutMapping
    public JsonData update(@RequestBody StorageMgt storageMgt){
        storageMgtService.update(storageMgt);
        return GlobalConfig.LOCAL.get();
    }

    @PostMapping
    public JsonData insert(@RequestBody StorageMgt storageMgt){
        storageMgtService.insert(storageMgt);
        return GlobalConfig.LOCAL.get();
    }

}
