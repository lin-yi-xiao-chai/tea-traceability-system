package com.powernode.teatraceability.controller.circulationprocessmgt;


import com.powernode.teatraceability.pojo.circulationprocessmgt.TransportInfo;
import com.powernode.teatraceability.pojo.login.User;
import com.powernode.teatraceability.service.circulationprocessmgt.TransportInfoService;
import com.powernode.teatraceability.service.circulationprocessmgt.impl.TransportInfoServiceImpl;
import com.powernode.teatraceability.util.GlobalConfig;
import com.powernode.teatraceability.util.JsonData;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

@RestController
@RequestMapping("/transport/info")
public class TransportInfoController {

    @Resource
    private TransportInfoService transportInfoService;

    @GetMapping("/{shiperName}/{pageNum}/{pageSize}")
    public JsonData selectLike(@PathVariable int pageNum,
                              @PathVariable int pageSize,
                              @PathVariable String shiperName,
                              @SessionAttribute User user){
        GlobalConfig.initPageRequest(pageNum, pageSize);
        transportInfoService.selectLike(user.getQydm(), shiperName);
        return GlobalConfig.LOCAL.get();
    }

    @GetMapping("/{pageNum}/{pageSize}")
    public JsonData selectAll(@PathVariable int pageNum,
                              @PathVariable int pageSize,
                              @SessionAttribute User user){
        GlobalConfig.initPageRequest(pageNum, pageSize);
        transportInfoService.selectAll(user.getQydm());
        return GlobalConfig.LOCAL.get();
    }

    @PostMapping
    public JsonData insert(@RequestBody TransportInfo transportInfo,
                           @SessionAttribute User user){
        transportInfo.setQydm(user.getQydm());
        transportInfoService.insert(transportInfo);
        return GlobalConfig.LOCAL.get();
    }

    @PutMapping
    public JsonData update(@RequestBody TransportInfo transportInfo){
        transportInfoService.update(transportInfo);
        return GlobalConfig.LOCAL.get();
    }


}
