package com.powernode.teatraceability.controller.circulationprocessmgt;


import com.powernode.teatraceability.pojo.circulationprocessmgt.TransactionInfo;
import com.powernode.teatraceability.pojo.login.User;
import com.powernode.teatraceability.service.circulationprocessmgt.TransactionInfoService;
import com.powernode.teatraceability.util.GlobalConfig;
import com.powernode.teatraceability.util.JsonData;
import org.apache.ibatis.annotations.Delete;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

@RestController
@RequestMapping("/transaction/info")
public class TransactionInfoController {

    @Resource
    private TransactionInfoService transactionInfoService;

    @GetMapping("/{name}/{pageNum}/{pageSize}")
    public JsonData selectLike(@PathVariable int pageNum,
                              @PathVariable int pageSize,
                              @PathVariable String name,
                              @SessionAttribute User user){
        GlobalConfig.initPageRequest(pageNum,pageSize);
        transactionInfoService.selectLike(user.getQydm(), name);
        return GlobalConfig.LOCAL.get();
    }

    @GetMapping("/{pageNum}/{pageSize}")
    public JsonData selectAll(@PathVariable int pageNum,
                              @PathVariable int pageSize,
                              @SessionAttribute User user){
        GlobalConfig.initPageRequest(pageNum,pageSize);
        transactionInfoService.selectAll(user.getQydm());
        return GlobalConfig.LOCAL.get();
    }
    @DeleteMapping("/{id}")
    public JsonData delete(@PathVariable int id){
        transactionInfoService.delete(id);
        return GlobalConfig.LOCAL.get();
    }

    @PostMapping
    public JsonData insert(@RequestBody TransactionInfo transactionInfo,
                           @SessionAttribute User user){
        transactionInfo.setQydm(user.getQydm());
        transactionInfoService.insert(transactionInfo);
        return GlobalConfig.LOCAL.get();
    }
    @PutMapping
    public JsonData update(@RequestBody TransactionInfo transactionInfo){
        transactionInfoService.update(transactionInfo);
        return GlobalConfig.LOCAL.get();
    }

}
