package com.powernode.teatraceability.controller.circulationprocessmgt;


import com.powernode.teatraceability.pojo.circulationprocessmgt.ClientInfo;
import com.powernode.teatraceability.pojo.login.User;
import com.powernode.teatraceability.service.circulationprocessmgt.ClientInfoService;
import com.powernode.teatraceability.util.GlobalConfig;
import com.powernode.teatraceability.util.JsonData;
import jdk.nashorn.internal.objects.Global;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

@RestController
@RequestMapping("/client/info")
public class ClientInfoController {

    @Resource
    private ClientInfoService clientInfoService;

    @GetMapping
    public JsonData selectAllClient(@SessionAttribute User user){
        clientInfoService.queryAllClient(user.getQydm());
        return GlobalConfig.LOCAL.get();
    }

    @GetMapping("/{clientName}/{pageNum}/{pageSize}")
    public JsonData selectLike(@PathVariable int pageNum,
                              @PathVariable int pageSize,
                              @PathVariable String clientName,
                              @SessionAttribute User user){
        GlobalConfig.initPageRequest(pageNum,pageSize);
        clientInfoService.selectLike(user.getQydm(), clientName);
        return GlobalConfig.LOCAL.get();
    }

    @GetMapping("/{pageNum}/{pageSize}")
    public JsonData selectAll(@PathVariable int pageNum,
                              @PathVariable int pageSize,
                              @SessionAttribute User user){
        GlobalConfig.initPageRequest(pageNum,pageSize);
        clientInfoService.selectAll(user.getQydm());
        return GlobalConfig.LOCAL.get();
    }

    @PostMapping
    public JsonData insert(@RequestBody ClientInfo clientInfo,
                           @SessionAttribute User user){
        clientInfo.setQydm(user.getQydm());
        clientInfoService.insert(clientInfo);
        return GlobalConfig.LOCAL.get();
    }

    @PutMapping
    public JsonData update(@RequestBody ClientInfo clientInfo){
        clientInfoService.update(clientInfo);
        return GlobalConfig.LOCAL.get();
    }

    @DeleteMapping("/{id}")
    public JsonData delete(@PathVariable int id){
        clientInfoService.delete(id);
        return GlobalConfig.LOCAL.get();
    }


}
