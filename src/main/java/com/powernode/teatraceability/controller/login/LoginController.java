package com.powernode.teatraceability.controller.login;


import com.powernode.teatraceability.pojo.Account;
import com.powernode.teatraceability.service.user.UserService;
import com.powernode.teatraceability.util.GlobalConfig;
import com.powernode.teatraceability.util.JsonData;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

@RestController
public class LoginController {

    @Resource
    private UserService userService;

    @PostMapping("/user/login")
    public JsonData login(@RequestBody Account account,
                          HttpSession session){

        JsonData jsonData = new JsonData();

        if(userService.verify(account.getUsername(),account.getPassword(),account.getGroupName())){
            session.setAttribute("user", GlobalConfig.getUser());
            jsonData.setStatus(200);
            jsonData.setMsg("登陆成功");
        }else{
            jsonData.setStatus(400);
            jsonData.setMsg("账号或密码有误");
        }
        return jsonData;
    }

}
