package com.powernode.teatraceability.controller.basicinfomgt;

import com.powernode.teatraceability.pojo.basicinfomgt.Product;
import com.powernode.teatraceability.pojo.login.User;
import com.powernode.teatraceability.service.basicinfomgt.ProductService;
import com.powernode.teatraceability.util.GlobalConfig;
import com.powernode.teatraceability.util.JsonData;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

@RestController
@RequestMapping("/product")
public class ProductController {

    @Resource
    private ProductService productService;

    @GetMapping
    public JsonData selectAllProduct(){
        List<String> productNames = productService.queryAllProduct();
        JsonData jsonData = GlobalConfig.LOCAL.get();
        jsonData.setData(productNames);
        jsonData.setStatus(200);
        jsonData.setMsg("ok");
        return jsonData;
    }

    @GetMapping("/{pageNum}/{pageSize}")
    public JsonData selectAllByQydm(@SessionAttribute User user,
                                      @PathVariable int pageNum,
                                      @PathVariable int pageSize){
        GlobalConfig.initPageRequest(pageNum,pageSize);
        productService.selectAllByQydm(user.getQydm());
        return GlobalConfig.LOCAL.get();
    }
    @GetMapping("/{cpmc}/{pageNum}/{pageSize}")
    public JsonData selectLikeByChanpinName(
                                       @PathVariable String cpmc,
                                       @SessionAttribute User user,
                                       @PathVariable int pageNum,
                                       @PathVariable int pageSize){
        GlobalConfig.initPageRequest(pageNum,pageSize);
        productService.selectLikeByChanpinName(cpmc,user.getQydm());
        return GlobalConfig.LOCAL.get();
    }
    @DeleteMapping("/{bh}")
    public JsonData deleteByBh(@PathVariable int bh){
        productService.deleteByBh(bh);
        return GlobalConfig.LOCAL.get();
    }
    @PutMapping
    public JsonData update(@RequestBody Product product){
        productService.update(product);
        return GlobalConfig.LOCAL.get();
    }
    @PostMapping
    public JsonData insert(@RequestBody Product product,@SessionAttribute User user){
        product.setQydm(user.getQydm());
        product.setCpbh(productService.getOneCpbh());
        productService.insert(product);
        return GlobalConfig.LOCAL.get();
    }



}
