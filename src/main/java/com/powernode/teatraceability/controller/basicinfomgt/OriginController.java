package com.powernode.teatraceability.controller.basicinfomgt;


import com.powernode.teatraceability.exceptions.InsertException;
import com.powernode.teatraceability.pojo.basicinfomgt.Origin;
import com.powernode.teatraceability.pojo.page.PageResult;
import com.powernode.teatraceability.pojo.login.User;
import com.powernode.teatraceability.service.basicinfomgt.OriginService;
import com.powernode.teatraceability.util.GlobalConfig;
import com.powernode.teatraceability.util.JsonData;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

@RestController
@RequestMapping("/chandi")
public class OriginController {

    @Resource
    private OriginService originService;


    @GetMapping
    public JsonData selectAllChandi(@SessionAttribute User user){
        originService.queryAllChandi(user.getQydm());
        return GlobalConfig.LOCAL.get();
    }

    @GetMapping("/{chandi}/{pageNum}/{pageSize}")
    public JsonData<PageResult> selectLikeByAddress(@PathVariable String chandi,
                                        @SessionAttribute User user,
                                        @PathVariable int pageNum,
                                        @PathVariable int pageSize ){
        GlobalConfig.initPageRequest(pageNum,pageSize);
        originService.selectByChanDi(chandi,user.getQydm());
        return GlobalConfig.LOCAL.get();
    }
    @GetMapping("/{pageNum}/{pageSize}")
    public JsonData<PageResult> selectByQydm(@PathVariable int pageNum,
                                     @PathVariable int pageSize,
                                     @SessionAttribute User user){
        GlobalConfig.initPageRequest(pageNum,pageSize);
        originService.selectByQydm(user.getQydm());
        return GlobalConfig.LOCAL.get();
    }
    @PutMapping
    public JsonData update(@RequestBody Origin orgin){
        originService.update(orgin);
        return GlobalConfig.LOCAL.get();
    }
    @DeleteMapping("/{bh}")
    public JsonData delete(@PathVariable String bh){
        originService.deleteByBh(bh);
        return GlobalConfig.LOCAL.get();
    }
    @PostMapping
    public JsonData insert(@RequestBody Origin origin,
                           @SessionAttribute User user) throws InsertException {
        origin.setQydm(user.getQydm());
        String ans = String.valueOf((int) (Math.random() * 100000));
        origin.setCdbh(ans);
        originService.insert(origin);
        return GlobalConfig.LOCAL.get();
    }

}
