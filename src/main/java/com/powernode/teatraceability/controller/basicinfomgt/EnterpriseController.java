package com.powernode.teatraceability.controller.basicinfomgt;

import com.powernode.teatraceability.pojo.basicinfomgt.Enterprise;
import com.powernode.teatraceability.pojo.login.User;
import com.powernode.teatraceability.service.basicinfomgt.EnterpriseService;
import com.powernode.teatraceability.util.GlobalConfig;
import com.powernode.teatraceability.util.JsonData;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

@RestController
@RequestMapping("/qy")
public class EnterpriseController {

    @Resource
    private EnterpriseService enterpriseService;

    @GetMapping("/enterpriseinfo")
    public JsonData selectEnterpriseInfo(@SessionAttribute User user){
        enterpriseService.queryByQydm(user.getQydm());
        return GlobalConfig.LOCAL.get();
    }
    @PutMapping
    public JsonData update(@RequestBody Enterprise enterprise,
                       @SessionAttribute User user){
        enterpriseService.updateEnterprise(enterprise,user.getQydm());
        return GlobalConfig.LOCAL.get();
    }

}
