package com.powernode.teatraceability.exceptions;

public class InsertException extends Exception{
    public InsertException(String message) {
        super(message);
    }

    public InsertException() {
    }
}
