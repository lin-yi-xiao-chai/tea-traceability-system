package com.powernode.teatraceability;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@SpringBootApplication
@MapperScan("com.powernode.teatraceability.dao")
@EnableTransactionManagement
@EnableAspectJAutoProxy
public class TeaTraceabilityApplication {

    public static void main(String[] args) {
        SpringApplication.run(TeaTraceabilityApplication.class, args);
    }

}
