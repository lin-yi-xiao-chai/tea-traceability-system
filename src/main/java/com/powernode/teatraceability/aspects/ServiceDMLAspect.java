package com.powernode.teatraceability.aspects;

import com.powernode.teatraceability.util.GlobalConfig;
import com.powernode.teatraceability.util.JsonData;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

/**
 * 业务逻辑层从数据库中收集到的数据
 * 整合到成JsonData对象封装起来，准备发给前端
 */
@Aspect
@Component
public class ServiceDMLAspect {

    @Pointcut("execution(public * com.powernode.teatraceability.service.sourceinfo.impl.*.insert(..))")
    public void insertSource(){}

    @Pointcut("execution(public * com.powernode.teatraceability.service.basicinfomgt.impl.ProductServiceImpl.insert(..))")
    public void insertProduct(){}
    @Pointcut("execution(public * com.powernode.teatraceability.service.basicinfomgt.impl.OriginServiceImpl.insert(*))")
    public void insertOrigin(){}

    /**
     * 种植过程管理插入
     */
    @Pointcut("execution(public * com.powernode.teatraceability.service.plateprocessmgt.impl.*.insert(..))")
    public void insertInput(){}

    @Pointcut("execution(public * com.powernode.teatraceability.service.processmgt.impl.*.insert(..)))")
    public void insertProcess(){}

    @Pointcut("execution(public * com.powernode.teatraceability.service.circulationprocessmgt.impl.*.insert(..))")
    public void insertCir(){}

    @Around("insertOrigin()||insertProduct()||insertInput()||insertProcess()||insertCir()||insertSource()")
    public void insertAdvice(ProceedingJoinPoint joinPoint){
        JsonData jsonData = GlobalConfig.LOCAL.get();
        try {
            Integer cnt = (Integer) joinPoint.proceed();
            if(cnt == 1){
                jsonData.setMsg("添加成功！");
                jsonData.setStatus(200);
            }else{
                jsonData.setMsg("添加失败,请检查你所添加的数据是否正确！");
                jsonData.setStatus(400);
            }
        }catch (Throwable e){
            jsonData.setMsg("添加失败！");
            jsonData.setStatus(500);
            e.printStackTrace();
        }
    }

    @Pointcut("execution(public * com.powernode.teatraceability.service.basicinfomgt.impl.ProductServiceImpl.deleteByBh(..))")
    public void deleteProduct(){}
    @Pointcut("execution(public * com.powernode.teatraceability.service.basicinfomgt.impl.OriginServiceImpl.deleteByBh(*))")
    public void deleteOrigin(){}

    /**
     * 种植管理系统删除
     */
    @Pointcut("execution(public * com.powernode.teatraceability.service.plateprocessmgt.impl.*.delete(..))")
    public void deleteInput(){}

    @Pointcut("execution(public * com.powernode.teatraceability.service.processmgt.impl.*.delete*(..))")
    public void deleteProcess(){}

    @Pointcut("execution(public * com.powernode.teatraceability.service.circulationprocessmgt.impl.*.delete*(..))")
    public void deleteCir(){}

    @Pointcut("execution(public * com.powernode.teatraceability.service.sourceinfo.impl.*.delete*(..))")
    public void deleteSource(){}

    @Around("deleteProduct()||deleteOrigin()||deleteInput()||deleteProcess()||deleteCir()||deleteSource()")
    public void deleteAdvice(ProceedingJoinPoint joinPoint){
        JsonData jsonData = GlobalConfig.LOCAL.get();
        try {
            Integer cnt = (Integer) joinPoint.proceed();
            if(cnt == 1){
                jsonData.setMsg("删除成功！");
                jsonData.setStatus(200);
            }else{
                jsonData.setMsg("删除失败,请重新尝试");
                jsonData.setStatus(400);
            }
        }catch (Throwable e){
            jsonData.setMsg("删除失败,请稍后");
            jsonData.setStatus(500);
            e.printStackTrace();
        }
    }

    @Pointcut("execution(public * com.powernode.teatraceability.service.basicinfomgt.impl.ProductServiceImpl.update(..))")
    public void updateProduct(){}
    @Pointcut("execution(public * com.powernode.teatraceability.service.basicinfomgt.impl.OriginServiceImpl.update(*))")
    public void updateOrigin(){}

    /**
     * 种植过程管理更新
     */
    @Pointcut("execution(public * com.powernode.teatraceability.service.plateprocessmgt.impl.*.update(..))")
    public void updateInput(){}

    @Pointcut("execution(public * com.powernode.teatraceability.service.processmgt.impl.*.update*(..))")
    public void updateProcess(){}

    @Pointcut("execution(public * com.powernode.teatraceability.service.circulationprocessmgt.impl.*.update*(..))")
    public void updateCir(){}

    @Pointcut("execution(public * com.powernode.teatraceability.service.sourceinfo.impl.*.update*(..))")
    public void updateSource(){}

    @Around("updateOrigin()||updateProduct()||updateInput()||updateProcess()||updateCir()||updateSource()")
    public void updateAdvice(ProceedingJoinPoint joinPoint){
        JsonData jsonData = GlobalConfig.LOCAL.get();
        try {
            Integer cnt = (Integer) joinPoint.proceed();
            if(cnt == 1){
                jsonData.setMsg("编辑成功！");
                jsonData.setStatus(200);
            }else{
                jsonData.setMsg("编辑失败,请重新尝试");
                jsonData.setStatus(400);
            }
        }catch (Throwable e){
            jsonData.setMsg("编辑失败,请稍后");
            jsonData.setStatus(500);
            e.printStackTrace();
        }
    }

}
