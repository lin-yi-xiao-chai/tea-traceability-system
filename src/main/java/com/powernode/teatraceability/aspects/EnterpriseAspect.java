package com.powernode.teatraceability.aspects;

import com.powernode.teatraceability.util.GlobalConfig;
import com.powernode.teatraceability.util.JsonData;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

/**
 * 企业信息准备返回给前端的数据+投入品基本信息
 */
@Aspect
@Component
public class EnterpriseAspect {

    @Around("execution(public * com.powernode.teatraceability.service.basicinfomgt.impl.EnterpriseServiceImpl.update*(..))")
    public void updateAdvice(ProceedingJoinPoint joinPoint){
        JsonData jsonData = GlobalConfig.LOCAL.get();
        try {
            joinPoint.proceed();
            jsonData.setStatus(200);
            jsonData.setMsg("更新成功");
        } catch (Throwable e) {
            jsonData.setMsg("更新失败");
            jsonData.setStatus(500);
            e.printStackTrace();
        }
    }
    @Pointcut("execution(public * com.powernode.teatraceability.service.basicinfomgt.impl.EnterpriseServiceImpl.query*(..))")
    public void enterpriseQueryAdvice(){}
    @Pointcut("execution(public * com.powernode.teatraceability.service.plateprocessmgt.impl.InputServiceImpl.query*(..))")
    public void inputQueryAdvice(){}

    @Pointcut("execution(public * com.powernode.teatraceability.service.basicinfomgt.impl.OriginServiceImpl.queryAllChandi(..))")
    public void originQueryAdvice(){}

    @Pointcut("execution(public * com.powernode.teatraceability.service.processmgt.impl..query*(..))")
    public void processMgtAdvice(){}

    @Pointcut("execution(public * com.powernode.teatraceability.service.circulationprocessmgt.impl..query*(..))")
    public void cirProcessMgtAdvice(){}

    @Around("enterpriseQueryAdvice()||inputQueryAdvice()||originQueryAdvice()||processMgtAdvice()||cirProcessMgtAdvice()")
    public void queryAroundAdvice(ProceedingJoinPoint joinPoint){
        JsonData jsonData = GlobalConfig.LOCAL.get();
        Object data = null;
        try {
            data = joinPoint.proceed();
        } catch (Throwable e) {
            e.printStackTrace();
        }
        jsonData.setData(data);
        jsonData.setStatus(200);
        jsonData.setMsg("ok");
    }

}
