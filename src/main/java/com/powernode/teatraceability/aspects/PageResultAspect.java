package com.powernode.teatraceability.aspects;


import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.powernode.teatraceability.pojo.page.PageRequest;
import com.powernode.teatraceability.pojo.page.PageResult;
import com.powernode.teatraceability.util.GlobalConfig;
import com.powernode.teatraceability.util.JsonData;
import com.powernode.teatraceability.util.PageUtil;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;

import java.util.List;

@Aspect
@Component
public class PageResultAspect {

    @Pointcut("execution(public * com.powernode.teatraceability.service.circulationprocessmgt.impl..select*(..))")
    public void pageCir(){}

    @Pointcut("execution(public * com.powernode.teatraceability.service.processmgt.impl.*.select*(..))")
    public void pageProcess(){}


    /**
     * 种植过程管理下的所有select查询分页
     */
    @Pointcut("execution(public * com.powernode.teatraceability.service.plateprocessmgt.impl.*.select*(..))")
    public void pageInput(){}

    /**
     * 基本信息的产地分页
     */
    @Pointcut("execution(public * com.powernode.teatraceability.service.basicinfomgt.impl.OriginServiceImpl.select*(..))")
    public void pageOrigin(){}

    /**
     * 基本信息的产品分页
     */
    @Pointcut("execution(public * com.powernode.teatraceability.service.basicinfomgt.impl.ProductServiceImpl.select*(..))")
    public void pageProduct(){}

    @Pointcut("execution(public * com.powernode.teatraceability.service.sourceinfo.impl.*.select*(..))")
    public void pageSource(){}

    @Around("pageInput() || pageOrigin() || pageProduct() || pageProcess() || pageCir() || pageSource()")
    public void pageAroundAdvice(ProceedingJoinPoint joinPoint) {
        JsonData jsonData = GlobalConfig.LOCAL.get();
        try {
            // 获取分页参数
            PageRequest pageRequest = GlobalConfig.PAGE_REQUEST.get();
            // 开启分页
            if(pageRequest.getPageNum() != 0)
            PageHelper.startPage(pageRequest.getPageNum(), pageRequest.getPageSize());
            else{
                jsonData.setMsg("页码没有第0页哦");
                jsonData.setStatus(400);
                return;
            }
            // 得到分页数据
            List proceed = (List) joinPoint.proceed();
            // 得到分页结果
            PageResult pageResult = PageUtil.getPageResult(new PageInfo<>(proceed));
            // 处理显示数据
            jsonData.setData(pageResult);
            jsonData.setMsg("ok");
            jsonData.setStatus(200);
        }catch (Throwable e) {
            jsonData.setMsg("查询有误，请稍后");
            jsonData.setStatus(500);
            e.printStackTrace();
        }
    }

}
