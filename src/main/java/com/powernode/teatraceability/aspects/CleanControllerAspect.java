package com.powernode.teatraceability.aspects;

import com.powernode.teatraceability.util.GlobalConfig;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class CleanControllerAspect {

    /**
     * controller包下的所有类的所有方法
     */
    @Pointcut("execution(public com.powernode.teatraceability.util.JsonData com.powernode.teatraceability.controller..*(..))")
    public void all(){}


    @Before("all()")
    public void cleanAdvice(){
        // 清一下是因为Tomcat内发生请求是用的线程连接池
        // 不是每次都是新建的线程,所以线程可能抽到一样的，所以清一下吧.
        // emmmmmm....
        GlobalConfig.setEmptyJsonData();
    }

}
