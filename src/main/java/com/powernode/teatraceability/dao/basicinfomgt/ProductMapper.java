package com.powernode.teatraceability.dao.basicinfomgt;

import com.powernode.teatraceability.pojo.basicinfomgt.Product;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface ProductMapper {

    Product selectOneByCpbh(String cpbh);

    String selectGG(String cpmc);

    String selectCpmcByCpbh(String cpbh);

    List<String> selectAllProduct();

    String selectCpbhByCpmc(String cpmc);

    List<String> selectAllCpbh();

    /**
     *  通过企业代码查询所有产品信息
     * @return
     */
    List<Product> selectAllByQydm(String qydm);

    /**
     * 通过传过来的产品名称进行查询
     * @param cpmc
     * @param qydm
     * @return 模糊查询后的列表
     */
    List<Product> likeSelectByChanpinName(@Param("cpmc") String cpmc,
                                          @Param("qydm") String qydm);

    /**
     * 通过编号进行删除
     * @param bh
     * @return 删除条数
     */
    int deleteByBh(@Param("bh") int bh);

    /**
     * 编辑信息
     * @param product
     * @return
     */
    int update(Product product);

    /**
     * 添加产品信息
     * @param product
     * @return
     */
    int insert(Product product);


}
