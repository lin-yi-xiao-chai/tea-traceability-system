package com.powernode.teatraceability.dao.basicinfomgt;

import com.powernode.teatraceability.pojo.basicinfomgt.Enterprise;
import org.apache.ibatis.annotations.Param;

public interface EnterpriseMapper {

    /**
     * 获取企业名称
     * @param qydm
     * @return
     */
    String selectQYMCByQydm(@Param("qydm") String qydm);

    String selectFzrtelByQydm(String qydm);

    /**
     * 通过 bh 查找企业信息
     * @param qydm
     * @return
     */
    Enterprise selectOneByQydm(@Param("qydm") String qydm);

    /**
     * 更改企业相关信息（企业信息管理）
     * @param enterprise
     * @return
     */
    int update(Enterprise enterprise);

}
