package com.powernode.teatraceability.dao.basicinfomgt;

import com.powernode.teatraceability.pojo.basicinfomgt.Origin;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface OriginMapper {

    /*Origin selectOne(@Param("chandi") String chandi,
                     @Param("qydm") String qydm);*/

    String selectCdbhByCdmc(String cdmc);

    String selectCdmcByCdbh(String cdbh);

    /**
     * 模糊查询数据
     * @param chandi
     * @return
     */
    List<Origin> likeSelectByChanDi(@Param("chandi") String chandi,
                                     @Param("qydm") String qydm);

    /**
     * 查询企业编号为 qydm 的所有上传了的产地
     * @param qydm
     * @return
     */
    List<Origin> selectByQydm(@Param("qydm") String qydm);

    List<String> selectAllChandi(String qydm);

    /**
     * 编辑产地信息
     * @param origin
     * @return
     */
    int update(Origin origin);

    /**
     * 添加产地信息
     * @param origin
     * @return
     */
    int insert(Origin origin);

    /**
     * 根据地块名称进行删除
     * （软删除，改status字段为1即可）
     */
    int deleteByBh(@Param("Bh") String Bh);

}
