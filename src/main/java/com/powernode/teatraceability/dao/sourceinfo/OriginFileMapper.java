package com.powernode.teatraceability.dao.sourceinfo;

import com.powernode.teatraceability.pojo.sourceinfo.OriginFile;

public interface OriginFileMapper {


    OriginFile selectOne(String cdbh);

}
