package com.powernode.teatraceability.dao.sourceinfo;

import com.powernode.teatraceability.pojo.sourceinfo.ProductFile;

public interface ProductFileMapper {

    ProductFile selectOne(String pch);

}
