package com.powernode.teatraceability.dao.sourceinfo;

import com.powernode.teatraceability.pojo.sourceinfo.SourceCode;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface SourceCodeMapper {

    List<SourceCode> selectAll(String qydm);

    int insert(SourceCode sourceCode);

    int updateSmcs(@Param("bh") int bh,@Param("smcs") int smcs);
}
