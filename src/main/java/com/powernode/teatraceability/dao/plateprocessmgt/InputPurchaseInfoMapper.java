package com.powernode.teatraceability.dao.plateprocessmgt;

import com.powernode.teatraceability.pojo.plateprocessmgt.InputPurchaseInfo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface InputPurchaseInfoMapper {

    List<InputPurchaseInfo> selectLike(@Param("qydm") String qydm,@Param("trpmc") String trpmc);

    /**
     * 通过企业代码获取投入品采购信息
     * @param qydm
     * @return
     */
    List<InputPurchaseInfo> selectAllByQydm(String qydm);

    /**
     * 编辑投入品采购信息
     * @param inputPurchaseInfo
     * @return
     */
    int update(InputPurchaseInfo inputPurchaseInfo);

    /**
     * 通过 bh 主键对采购信息进行删除
     * @param bh
     * @return
     */
    int delete(int bh);

    /**
     * 新增投入品采购信息
     * @param inputPurchaseInfo
     * @return
     */
    int insert(InputPurchaseInfo inputPurchaseInfo);

}
