package com.powernode.teatraceability.dao.plateprocessmgt;

import com.powernode.teatraceability.pojo.plateprocessmgt.FarmWorkInfo;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

public interface FarmWorkInfoMapper {

    List<FarmWorkInfo> selectLike(@Param("qydm") String qydm,@Param("nszy") String nszy);


    // @Select("select * from t_nszy where qydm = #{qydm}")
    List<FarmWorkInfo> selectAllByQydm(String qydm);

    List<FarmWorkInfo> selectAllByZyr(String zyr);

    int insert(FarmWorkInfo farmWorkInfo);

    int delete(int bh);

    int update(FarmWorkInfo farmWorkInfo);


}
