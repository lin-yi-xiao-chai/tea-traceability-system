package com.powernode.teatraceability.dao.plateprocessmgt;

import com.powernode.teatraceability.pojo.plateprocessmgt.Input;
import org.apache.ibatis.annotations.Select;

public interface InputMapper {

    @Select("select * from t_trp where nydjzh=#{nydjzh}")
    Input selectByDjzh(String nydjzh);

}
