package com.powernode.teatraceability.dao.plateprocessmgt;

import com.powernode.teatraceability.pojo.plateprocessmgt.DetectionInfo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface DetectionInfoMapper {

    List<DetectionInfo> selectLike(@Param("qydm") String qydm,@Param("cpmc") String cpmc);

    DetectionInfo selectOneByPch(String pch);

    List<String> selectAllPch(@Param("ybmc") String ybmc,@Param("qydm") String qydm);

    List<DetectionInfo> selectAllByQydm(String qydm);

    int update(DetectionInfo detectionInfo);

    int delete(int bh);

    int insert(DetectionInfo detectionInfo);


}
