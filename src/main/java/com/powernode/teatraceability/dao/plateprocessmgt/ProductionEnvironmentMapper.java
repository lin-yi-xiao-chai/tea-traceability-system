package com.powernode.teatraceability.dao.plateprocessmgt;

import com.powernode.teatraceability.pojo.plateprocessmgt.ProductionEnvironment;
import org.apache.ibatis.annotations.Select;

import java.util.List;

public interface ProductionEnvironmentMapper {

    @Select("select * from t_schj")
    List<ProductionEnvironment> selectAll();

    @Select("select avg(ph) ph,avg(trwd) trwd, avg(trsd) trsd from t_schj group by chandi")
    ProductionEnvironment selectOne(String chandi);

}
