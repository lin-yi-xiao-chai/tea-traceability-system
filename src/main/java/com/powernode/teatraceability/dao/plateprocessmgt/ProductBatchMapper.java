package com.powernode.teatraceability.dao.plateprocessmgt;

import com.powernode.teatraceability.pojo.plateprocessmgt.ProductBatch;
import com.powernode.teatraceability.pojo.sourceinfo.BhInfo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface ProductBatchMapper {

    List<ProductBatch> selectLike(@Param("qydm") String qydm,@Param("cpmc") String cpmc);

    BhInfo selectBhInfo(String pch);

    String selectCpbhByPch(String pch);

    String selectCdbhByPch(String pch);

    String selectCpmcByPch(String pch);

    List<ProductBatch> selectAllInfo(String qydm);

    List<String> selectAllPch();

    int delete(int bh);

    int insert(ProductBatch productBatch);


}
