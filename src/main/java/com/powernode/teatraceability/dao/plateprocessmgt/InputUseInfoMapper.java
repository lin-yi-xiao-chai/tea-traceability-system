package com.powernode.teatraceability.dao.plateprocessmgt;

import com.powernode.teatraceability.pojo.plateprocessmgt.InputUseInfo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface InputUseInfoMapper {

    List<InputUseInfo> selectLikeByTrpmc(@Param("qydm") String qydm,@Param("trpmc") String trpmc);

    List<InputUseInfo> selectByQydm(String qydm);

    int update(InputUseInfo useInfo);

    int insert(InputUseInfo useInfo);

    int delete(int bh);

}
