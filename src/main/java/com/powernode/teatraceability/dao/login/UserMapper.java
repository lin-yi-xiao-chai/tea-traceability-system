package com.powernode.teatraceability.dao.login;

import com.powernode.teatraceability.pojo.login.User;
import org.apache.ibatis.annotations.Param;

public interface UserMapper {

    /**
     * 通过人员编号查询人员信息
     */
    User selectByUserId(@Param("rybh") String rybh);

}
