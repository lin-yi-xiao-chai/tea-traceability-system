package com.powernode.teatraceability.dao.circulationprocessmgt;

import com.powernode.teatraceability.pojo.circulationprocessmgt.TransportInfo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface TransportInfoMapper {

    List<TransportInfo> selectLike(@Param("qydm") String qydm,@Param("shiperName") String shiperName);

    List<TransportInfo> selectAll(String qydm);

    int update(TransportInfo transportInfo);

    int insert(TransportInfo transportInfo);
}
