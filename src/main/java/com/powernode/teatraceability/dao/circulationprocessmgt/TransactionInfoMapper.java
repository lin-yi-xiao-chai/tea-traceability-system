package com.powernode.teatraceability.dao.circulationprocessmgt;

import com.powernode.teatraceability.pojo.circulationprocessmgt.TransactionInfo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface TransactionInfoMapper {

    List<TransactionInfo> selectLike(@Param("qydm") String qydm,@Param("name") String name);

    List<TransactionInfo> selectAll(String qydm);

    int update(TransactionInfo transactionInfo);

    int insert(TransactionInfo transactionInfo);

    int delete(int id);


}
