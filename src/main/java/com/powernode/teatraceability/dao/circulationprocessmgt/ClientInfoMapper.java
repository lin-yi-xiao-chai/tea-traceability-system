package com.powernode.teatraceability.dao.circulationprocessmgt;

import com.powernode.teatraceability.pojo.circulationprocessmgt.ClientInfo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface ClientInfoMapper {

    List<ClientInfo> selectLike(@Param("qydm") String qydm,@Param("clientName") String clientName);

    Integer selectByClientName(String name);

    List<String> selectAllClient(String qydm);

    ClientInfo selectById(int id);

    List<ClientInfo> selectAll(String qydm);

    int insert(ClientInfo clientInfo);

    int update(ClientInfo clientInfo);

    int delete(int id);

}
