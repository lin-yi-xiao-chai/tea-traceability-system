package com.powernode.teatraceability.dao.processmgt;

import com.powernode.teatraceability.pojo.processmgt.InputStorageMgt;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface InputStorageMgtMapper {

    List<InputStorageMgt> selectLike(@Param("qydm") String qydm, @Param("supplyName") String supplyName);

    List<InputStorageMgt> selectAll(String qydm);

    int insert(InputStorageMgt inputStorageMgt);

    int update(InputStorageMgt inputStorageMgt);

    int delete(int id);

}
