package com.powernode.teatraceability.dao.processmgt;

import com.powernode.teatraceability.pojo.processmgt.InputStorageMgt;
import com.powernode.teatraceability.pojo.processmgt.OutputStorageMgt;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface OutputStorageMgtMapper {

    List<InputStorageMgt> selectLike(@Param("qydm")String qydm,@Param("cpmc") String cpmc);

    List<InputStorageMgt> selectAll(String qydm);

    int insert(OutputStorageMgt outputStorageMgt);

    int update(OutputStorageMgt outputStorageMgt);

    int delete(int id);

}
