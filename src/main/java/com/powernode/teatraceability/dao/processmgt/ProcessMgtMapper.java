package com.powernode.teatraceability.dao.processmgt;

import com.powernode.teatraceability.pojo.processmgt.ProcessMgt;
import org.apache.ibatis.annotations.Param;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

public interface ProcessMgtMapper {
    List<ProcessMgt> selectLike(@Param("qydm") String qydm,@Param("fzr") String fzr);

    int deleteById(int id);

    int update(ProcessMgt processMgt);

    int insert(ProcessMgt processMgt);

    List<ProcessMgt> selectAll(String qydm);

}
