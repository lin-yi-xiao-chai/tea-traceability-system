package com.powernode.teatraceability.dao.processmgt;

import com.powernode.teatraceability.pojo.processmgt.StorageMgt;

import java.util.List;

public interface StorageMgtMapper {

    List<StorageMgt> selectLike(String storageName);

    List<String> selectAllStorage();

    Integer selectIdByName(String name);

    String selectById(int id);

    List<StorageMgt> selectAll();

    int update(StorageMgt storageMgt);

    int delete(int id);

    int insert(StorageMgt storageMgt);

}
