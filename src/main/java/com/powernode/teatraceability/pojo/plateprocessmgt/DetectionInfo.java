package com.powernode.teatraceability.pojo.plateprocessmgt;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;

@Data
@NoArgsConstructor
public class DetectionInfo implements Serializable {
    private Integer bh;

    private String qymc;

    private String qydm;

    private String pch;

    private String jcnr;

    private String cpmc;

    private String jcjg;

    private String jcz;

    private String yjbz;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date rq;

    private String jcr;

    private String pic;

    private String jcdd;

    private String isyq;

}