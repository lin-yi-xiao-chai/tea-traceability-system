package com.powernode.teatraceability.pojo.plateprocessmgt;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;

@Data
@NoArgsConstructor
public class InputUseInfo implements Serializable {
    private Integer bh;

    private String qymc;

    private String qydm;

    private String trpmc;

    private String nydjzh;

    private String jsr;

    private String syqx;

    private String sydx;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date syrq;

    private String bz;

    private String trpgg;

    private String sysl;

    private String nysyfa;

    private String nyscz;

    private String nyfz;

    private String nyzj;

}