package com.powernode.teatraceability.pojo.plateprocessmgt;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;


@Data
@NoArgsConstructor
public class InputPurchaseInfo implements Serializable {
    private Integer bh;
    private String qymc;
    private String qydm;
    private String trpmc;
    private String trpgg;
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    private Date rq;
    private String jsr;
    private String bz;
    private String ly;
    private String nyscz;
    private String nydjzh;
    private String nyscdz;
    private String nyzj;
    private String nyzw;
    private String nyfz;
    private String nysyfa;
    private String nydh;
    private String pdclass;
    private String cgsl;

}