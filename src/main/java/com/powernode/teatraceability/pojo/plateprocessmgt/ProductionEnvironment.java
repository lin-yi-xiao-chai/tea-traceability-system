package com.powernode.teatraceability.pojo.plateprocessmgt;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;


@Data
@NoArgsConstructor
public class ProductionEnvironment implements Serializable {
    private Integer bh;

    private String qymc;

    private String qydm;

    private String chandi;

    private Double ph;

    private Double trwd;

    private Double trsd;

    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date gxrq;

}