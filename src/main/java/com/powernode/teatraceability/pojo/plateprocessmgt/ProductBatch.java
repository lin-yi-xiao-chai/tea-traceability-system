package com.powernode.teatraceability.pojo.plateprocessmgt;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;

@Data
@NoArgsConstructor
public class ProductBatch implements Serializable {
    private Integer bh;

    private String qymc;

    private String qydm;

    private String cpmc;

    private String cpgg;

    private String chandi;

    private String scfzr;

    private String pch;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date scrq;
    private String cpbh;

    private String cdbh;

}