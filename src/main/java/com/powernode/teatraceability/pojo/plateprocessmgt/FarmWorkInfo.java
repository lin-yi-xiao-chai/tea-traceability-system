package com.powernode.teatraceability.pojo.plateprocessmgt;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

@Data
@NoArgsConstructor
public class FarmWorkInfo implements Serializable {
    private Integer bh;

    private String qymc;

    private String qydm;

    private String nszy;

    private String trpmc;

    private BigDecimal zyl;

    private String zyldw;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date rq;

    private String zyr;

    private String cdbh;

    private String cdmc;
}