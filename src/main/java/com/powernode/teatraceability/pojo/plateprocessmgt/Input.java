package com.powernode.teatraceability.pojo.plateprocessmgt;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
public class Input implements Serializable {
    private Integer bh;

    private String nydjzh;

    private String trpmc;

    private String nyscz;

    private String trpgg;

    private String nysyfa;

    private String nyfz;

    private String nyzj;

}