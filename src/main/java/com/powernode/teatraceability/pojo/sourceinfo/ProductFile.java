package com.powernode.teatraceability.pojo.sourceinfo;


import com.powernode.teatraceability.pojo.basicinfomgt.Enterprise;
import com.powernode.teatraceability.pojo.basicinfomgt.Product;
import com.powernode.teatraceability.pojo.plateprocessmgt.DetectionInfo;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;
import java.util.List;

@Data
@NoArgsConstructor
public class ProductFile {

    private String qydm;
    private String pch;
    private String cpbh;
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date scrq;
    private Product product;
    private List<DetectionInfo> detectionInfo;
    private Enterprise enterprise;
}
