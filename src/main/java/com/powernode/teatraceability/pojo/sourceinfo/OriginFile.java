package com.powernode.teatraceability.pojo.sourceinfo;

import com.powernode.teatraceability.pojo.plateprocessmgt.ProductionEnvironment;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class OriginFile {

    //private String cdbh;
    private String chandi;
    private String detailAddress;
    private Double lng;
    private Double lat;
    private String cdt;
    private ProductionEnvironment productionEnvironment;

}
