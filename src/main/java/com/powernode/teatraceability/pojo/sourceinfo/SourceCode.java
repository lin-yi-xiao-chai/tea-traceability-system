package com.powernode.teatraceability.pojo.sourceinfo;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;

@Data
@NoArgsConstructor
public class SourceCode implements Serializable {
    private Integer bh;

    private String qydm;

    private String cdbh;

    private String chandi;

    private String cpbh;

    private String cpmc;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date date;

    private String sym;

    private String pch;

    private Integer smcs;

    private Integer dyfs;

    private Integer type;

    private String fzrtel;

}