package com.powernode.teatraceability.pojo.sourceinfo;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class BhInfo {

    private String pch;
    private String cpbh;
    private String cdbh;

}
