package com.powernode.teatraceability.pojo.sourceinfo;

import com.powernode.teatraceability.dao.sourceinfo.OriginFileMapper;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class Record {

    private OriginFile originFile;

    private ProductFile productFile;

}
