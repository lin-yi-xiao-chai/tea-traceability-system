package com.powernode.teatraceability.pojo.login;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

/**
 * 用户信息类
 * 用于登陆验证、联系其他表等操作
 */
@Data
@NoArgsConstructor
public class User implements Serializable {
    private Integer id;
    /**
     * 用户名
     */
    private String rybh;
    /**
     * 用户姓名（实名）
     */
    private String rymc;
    /**
     * 密码（口令）
     */
    private String kl;
    /**
     * 组名称（企业、xx管理员等）
     */
    private String groupname;

    private String dh;

    private Date birth;

    private String sex;
    /**
     * 企业代码
     */
    private String qydm;

    private Integer status;

    private String activationCode;

}