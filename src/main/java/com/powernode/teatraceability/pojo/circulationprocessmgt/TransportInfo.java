package com.powernode.teatraceability.pojo.circulationprocessmgt;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;


@Data
@NoArgsConstructor
public class TransportInfo implements Serializable {
    private Integer id;

    private String qydm;

    private String pch;

    private String number;

    private String shiperName;

    private String shiperPhone;

    private String shipAddress;

    private String destination;

    private String accepter;

    private Integer clientId;

    private String carrier;

    private String carrierPhone;

    private String license;

    private String courierNumber;

    private String waybillPic;

    private String cargoPic;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date deliveryTime;

    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;

    private String orderNumber;

    private ClientInfo clientInfo;

    private String clientName;

    private String cpmc;
}