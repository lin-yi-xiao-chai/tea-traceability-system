package com.powernode.teatraceability.pojo.circulationprocessmgt;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonTypeId;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;


@Data
@NoArgsConstructor
public class ClientInfo implements Serializable {
    private Integer id;

    private String qydm;

    private String clientFirm;

    private String clientName;

    private String clientPhone;

    private String clientAddress;

    private String clientSource;

    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;


}