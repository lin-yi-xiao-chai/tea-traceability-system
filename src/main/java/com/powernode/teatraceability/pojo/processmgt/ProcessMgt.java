package com.powernode.teatraceability.pojo.processmgt;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;

@Data
@NoArgsConstructor
public class ProcessMgt implements Serializable {
    private Integer id;

    private String qymc;

    private String qydm;

    private String cpmc;

    private String pch;

    private String cpbh;

    private String processName;

    private String fzr;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date processTime;

    private String processWay;

    private String processPic;

    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;

}