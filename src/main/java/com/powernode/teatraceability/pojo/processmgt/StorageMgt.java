package com.powernode.teatraceability.pojo.processmgt;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;

@Data
@NoArgsConstructor
public class StorageMgt implements Serializable {
    private Integer storageId;

    private String storageName;

    private String storageDh;

    private String storageAddr;

    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date cjsj;

    private String storageRl;

    private String storageFzr;


}