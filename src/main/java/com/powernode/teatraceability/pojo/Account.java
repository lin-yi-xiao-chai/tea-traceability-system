package com.powernode.teatraceability.pojo;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class Account {

    private String username;
    private String password;
    private String groupName;

}
