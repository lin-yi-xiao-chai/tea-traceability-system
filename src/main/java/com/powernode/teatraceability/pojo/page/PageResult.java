package com.powernode.teatraceability.pojo.page;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
public class PageResult{

    /**
     * 页码
     */
    private int pageNum;
    /**
     * 页面条数
     */
    private int pageSize;
    /**
     * 总条数,和pageinfo中的类型相互对应
     */
    private long totalSize;
    /**
     * 总页码
     */
    private int totalNum;
    /**
     * 数据信息
     */
    private long total;
    private List<?> info;

}
