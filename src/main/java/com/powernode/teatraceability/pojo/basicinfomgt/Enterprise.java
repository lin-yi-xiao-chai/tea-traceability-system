package com.powernode.teatraceability.pojo.basicinfomgt;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;

import java.io.Serializable;

@Data
@NoArgsConstructor
public class Enterprise implements Serializable {
    private Integer bh;

    private String qydm;

    private String qymc;

    private String fzr;

    private String fzrtel;

    private String addr;

    private String isqt;

    private String isdlbs;

    private String isyouji;

    private String isgreen;

    private String iswgh;

    private String jcxx;

    private String xydm;

    private String qylx;

    private String zyyw;

    private String zycp;

    private String jygm;

    private String taobaourl;

    private String tmallurl;

    private String wxurl;

    private String myurl;

    private String flag;

    private String zcrq;

    private String hjpic;

    private String jcpic;

    private String wghpic;

    private String lspic;

    private String yjpic;

    private String dlpic;

    private String qtpic;

    private String dktj;

    private String cptj;

    private String nszytj;

    private String trpcgtj;

    private String ycltj;

    private String jcxxtj;

    private String symtj;

    private Integer tj;

    private Integer newsum;

    private String vidurl;

    private String cardX;

    private String cardY;

    private String businessLicense;

    private String dwjj;

    private String hjqk;
}