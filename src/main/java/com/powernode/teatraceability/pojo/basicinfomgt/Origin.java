package com.powernode.teatraceability.pojo.basicinfomgt;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 产地信息
 *      对应基础信息管理中的上传土地信息模块
 *      （status作为标记字段，起软删除的作用，为1这条就删除了，为0这条就还没删除）
 */
@Data
@NoArgsConstructor
public class Origin implements Serializable {
    private Integer bh;

    private String qymc;

    private String qydm;

    private String cdbh;

    private String chandi;

    private BigDecimal lng;

    private BigDecimal lat;

    private String yt;

    private BigDecimal tiliang;

    private String jldw;

    private Byte status;

    /**
     * 产地名
     */
    private String detailaddress;
    /**
     * 产地图
     */
    private String cdt;
}