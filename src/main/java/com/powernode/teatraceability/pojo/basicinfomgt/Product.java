package com.powernode.teatraceability.pojo.basicinfomgt;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
public class Product implements Serializable {
    private Integer bh;

    private String qydm;

    private String qymc;

    private String cpmc;

    private String cpbh;

    private String cptp;

    private String cpgg;

    private Integer cpbzq;

    private String cpjs;
}