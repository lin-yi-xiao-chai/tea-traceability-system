package com.powernode.teatraceability.service.sourceinfo;

import com.powernode.teatraceability.util.JsonData;

public interface RecordService {

    JsonData getRecordInfo(String pch);

}
