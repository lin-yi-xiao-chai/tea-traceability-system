package com.powernode.teatraceability.service.sourceinfo.impl;

import com.powernode.teatraceability.dao.basicinfomgt.EnterpriseMapper;
import com.powernode.teatraceability.dao.sourceinfo.SourceCodeMapper;
import com.powernode.teatraceability.pojo.sourceinfo.SourceCode;
import com.powernode.teatraceability.service.sourceinfo.SourceCodeService;
import com.powernode.teatraceability.util.GlobalConfig;
import com.powernode.teatraceability.util.JsonData;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

@Service
@Transactional
public class SourceCodeServiceImpl implements SourceCodeService {

    @Resource
    private SourceCodeMapper sourceCodeMapper;

    @Resource
    private EnterpriseMapper enterpriseMapper;

    @Override
    public List<SourceCode> selectAll(String qydm) {
        return sourceCodeMapper.selectAll(qydm);
    }

    @Override
    public Integer iinsert(SourceCode sourceCode) {
        JsonData jsonData = GlobalConfig.LOCAL.get();
        sourceCode.setFzrtel(enterpriseMapper.selectFzrtelByQydm(sourceCode.getQydm()));
        jsonData.setData(sourceCode);
        int cnt = 0;
        try {
            cnt = sourceCodeMapper.insert(sourceCode);
            if(cnt==0){
                jsonData.setMsg("前端输入数据有误");
                jsonData.setStatus(400);
            }else{
                jsonData.setMsg("添加成功");
                jsonData.setStatus(200);
            }
        }catch(Exception e){
            jsonData.setMsg("添加失败");
            jsonData.setStatus(500);
            e.printStackTrace();
        }finally {
            return cnt;
        }

    }

    @Override
    public Integer updateSmcs(int bh, int smcs) {
        return sourceCodeMapper.updateSmcs(bh,smcs);
    }
}
