package com.powernode.teatraceability.service.sourceinfo;

import com.powernode.teatraceability.pojo.sourceinfo.SourceCode;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface SourceCodeService {

    List<SourceCode> selectAll(String qydm);

    Integer iinsert(SourceCode sourceCode);

    Integer updateSmcs(int bh, int smcs);

}
