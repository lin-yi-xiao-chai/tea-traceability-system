package com.powernode.teatraceability.service.sourceinfo.impl;

import com.powernode.teatraceability.dao.basicinfomgt.OriginMapper;
import com.powernode.teatraceability.dao.plateprocessmgt.ProductBatchMapper;
import com.powernode.teatraceability.dao.sourceinfo.OriginFileMapper;
import com.powernode.teatraceability.dao.sourceinfo.ProductFileMapper;
import com.powernode.teatraceability.pojo.sourceinfo.BhInfo;
import com.powernode.teatraceability.pojo.sourceinfo.OriginFile;
import com.powernode.teatraceability.pojo.sourceinfo.ProductFile;
import com.powernode.teatraceability.pojo.sourceinfo.Record;
import com.powernode.teatraceability.service.sourceinfo.RecordService;
import com.powernode.teatraceability.util.GlobalConfig;
import com.powernode.teatraceability.util.JsonData;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

@Service
public class RecordServiceImpl implements RecordService {

    @Resource
    private ProductBatchMapper productBatchMapper;
    @Resource
    private OriginFileMapper originFileMapper;
    @Resource
    private ProductFileMapper productFileMapper;

    @Override
    @Transactional(readOnly = true)
    public JsonData getRecordInfo(String pch) {
        JsonData jsonData = GlobalConfig.LOCAL.get();
        Record record = new Record();
        try {
            BhInfo bhInfo = productBatchMapper.selectBhInfo(pch);
            record.setOriginFile(originFileMapper.selectOne(bhInfo.getCdbh()));
            record.setProductFile(productFileMapper.selectOne(pch));
            jsonData.setStatus(200);
            jsonData.setMsg("档案查询成功");
            jsonData.setData(record);
        }catch(Exception e){
            jsonData.setStatus(500);
            jsonData.setMsg("信息有误，请稍后");
            e.printStackTrace();
        }finally {
            return jsonData;
        }
    }
}
