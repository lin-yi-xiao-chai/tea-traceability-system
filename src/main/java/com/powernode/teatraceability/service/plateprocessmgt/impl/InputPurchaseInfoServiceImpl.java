package com.powernode.teatraceability.service.plateprocessmgt.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.powernode.teatraceability.dao.basicinfomgt.EnterpriseMapper;
import com.powernode.teatraceability.dao.plateprocessmgt.InputPurchaseInfoMapper;
import com.powernode.teatraceability.pojo.page.PageRequest;
import com.powernode.teatraceability.pojo.page.PageResult;
import com.powernode.teatraceability.pojo.plateprocessmgt.InputPurchaseInfo;
import com.powernode.teatraceability.service.plateprocessmgt.InputPurchaseInfoService;
import com.powernode.teatraceability.util.PageUtil;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

@Service
@Transactional
public class InputPurchaseInfoServiceImpl implements InputPurchaseInfoService {

    @Resource
    private InputPurchaseInfoMapper inputPurchaseInfoMapper;

    @Resource
    private EnterpriseMapper enterpriseMapper;


    @Override
    public List<InputPurchaseInfo> selectLike(String qydm, String trpmc) {
        List<InputPurchaseInfo> inputPurchaseInfos = inputPurchaseInfoMapper.selectLike(qydm, trpmc);
        inputPurchaseInfos.forEach(inputPurchaseInfo -> inputPurchaseInfo.setQymc(enterpriseMapper.selectQYMCByQydm(inputPurchaseInfo.getQydm())));
        return inputPurchaseInfos;
    }

    /**
     * 获取分页数据
     * @param qydm
     * @return
     */
    @Override
    @Transactional(readOnly = true)
    public List<InputPurchaseInfo> selectAllByQydm(String qydm) {
        List<InputPurchaseInfo> inputPurchaseInfos = inputPurchaseInfoMapper.selectAllByQydm(qydm);
        addQymc(qydm,inputPurchaseInfos);
        return inputPurchaseInfos;
    }

    /**
     * 给企业名称属性赋值
     * @param qydm
     * @param inputPurchaseInfos
     */
    private void addQymc(String qydm,List<InputPurchaseInfo> inputPurchaseInfos){
        final String qymc = enterpriseMapper.selectQYMCByQydm(qydm);
        inputPurchaseInfos.forEach(inputPurchaseInfo -> inputPurchaseInfo.setQymc(qymc));
    }

    @Override
    public Integer update(InputPurchaseInfo inputPurchaseInfo) {
        return inputPurchaseInfoMapper.update(inputPurchaseInfo);
    }

    @Override
    public Integer delete(int bh) {
        return inputPurchaseInfoMapper.delete(bh);
    }

    @Override
    public Integer insert(InputPurchaseInfo inputPurchaseInfo) {
        return inputPurchaseInfoMapper.insert(inputPurchaseInfo);
    }
}
