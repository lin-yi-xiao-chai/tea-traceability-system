package com.powernode.teatraceability.service.plateprocessmgt.impl;

import com.powernode.teatraceability.dao.basicinfomgt.EnterpriseMapper;
import com.powernode.teatraceability.dao.plateprocessmgt.DetectionInfoMapper;
import com.powernode.teatraceability.pojo.plateprocessmgt.DetectionInfo;
import com.powernode.teatraceability.service.plateprocessmgt.DetectionInfoService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.Resource;
import java.util.List;

@Service
@Transactional
public class DetectionInfoServiceImpl implements DetectionInfoService {

    @Resource
    private DetectionInfoMapper detectionInfoMapper;

    @Resource
    private EnterpriseMapper enterpriseMapper;

    @Override
    public List<DetectionInfo> selectLike(String qydm, String cpmc) {
        List<DetectionInfo> detectionInfos = detectionInfoMapper.selectLike(qydm, cpmc);
        final String qymc = enterpriseMapper.selectQYMCByQydm(qydm);
        detectionInfos.forEach(detectionInfo -> detectionInfo.setQymc(qymc));
        return detectionInfos;
    }

    @Override
    public List<String> queryAllPch(String ybmc,String qydm) {
        return detectionInfoMapper.selectAllPch(ybmc,qydm);
    }

    @Override
    @Transactional(readOnly = true)
    public List<DetectionInfo> selectAllByQydm(String qydm) {
        List<DetectionInfo> detectionInfos = detectionInfoMapper.selectAllByQydm(qydm);
        final String qymc = enterpriseMapper.selectQYMCByQydm(qydm);
        detectionInfos.forEach(detectionInfo -> {
            detectionInfo.setQymc(qymc);
        });
        return detectionInfos;
    }

    @Override
    public Integer update(DetectionInfo detectionInfo) {
        return detectionInfoMapper.update(detectionInfo);
    }

    @Override
    public Integer delete(int bh) {
        return detectionInfoMapper.delete(bh);
    }

    @Override
    public Integer insert(DetectionInfo detectionInfo) {
        return detectionInfoMapper.insert(detectionInfo);
    }
}
