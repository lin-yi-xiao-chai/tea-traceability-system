package com.powernode.teatraceability.service.plateprocessmgt.impl;

import com.powernode.teatraceability.dao.basicinfomgt.EnterpriseMapper;
import com.powernode.teatraceability.dao.plateprocessmgt.InputUseInfoMapper;
import com.powernode.teatraceability.pojo.plateprocessmgt.InputUseInfo;
import com.powernode.teatraceability.service.plateprocessmgt.InputUseInfoService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

@Service
@Transactional
public class InputUseInfoServiceImpl implements InputUseInfoService {

    @Resource
    private InputUseInfoMapper inputUseInfoMapper;

    @Resource
    private EnterpriseMapper enterpriseMapper;

    @Override
    public List<InputUseInfo> selectLikeByTrpmc(String qydm, String trpmc) {
        List<InputUseInfo> inputUseInfos = inputUseInfoMapper.selectLikeByTrpmc(qydm, trpmc);
        inputUseInfos.forEach(inputUseInfo -> inputUseInfo.setQymc(enterpriseMapper.selectQYMCByQydm(inputUseInfo.getQydm())));
        return inputUseInfos;
    }

    @Override
    @Transactional(readOnly = true)
    public List<InputUseInfo> selectByQydm(String qydm) {
        List<InputUseInfo> inputUseInfos = inputUseInfoMapper.selectByQydm(qydm);
        final String qymc = enterpriseMapper.selectQYMCByQydm(qydm);
        inputUseInfos.forEach(inputUseInfo->inputUseInfo.setQymc(qymc));
        return inputUseInfos;
    }

    @Override
    public Integer update(InputUseInfo useInfo) {
        return inputUseInfoMapper.update(useInfo);
    }

    @Override
    public Integer insert(InputUseInfo useInfo) {
        return inputUseInfoMapper.insert(useInfo);
    }

    @Override
    public Integer delete(int bh) {
        return inputUseInfoMapper.delete(bh);
    }
}
