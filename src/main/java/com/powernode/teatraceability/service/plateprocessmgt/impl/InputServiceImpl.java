package com.powernode.teatraceability.service.plateprocessmgt.impl;

import com.powernode.teatraceability.dao.plateprocessmgt.InputMapper;
import com.powernode.teatraceability.pojo.plateprocessmgt.Input;
import com.powernode.teatraceability.service.plateprocessmgt.InputService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

@Service
public class InputServiceImpl implements InputService {
    @Resource
    private InputMapper inputMapper;

    @Override
    @Transactional(readOnly = true)
    public Input queryByDjzh(String nydjzh) {
        return inputMapper.selectByDjzh(nydjzh);
    }
}
