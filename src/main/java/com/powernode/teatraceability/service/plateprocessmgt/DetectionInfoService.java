package com.powernode.teatraceability.service.plateprocessmgt;

import com.powernode.teatraceability.pojo.plateprocessmgt.DetectionInfo;

import java.util.List;

public interface DetectionInfoService {

    List<DetectionInfo> selectLike(String qydm,String cpmc);

    List<String> queryAllPch(String ybmc,String qydm);

    List<DetectionInfo> selectAllByQydm(String qydm);

    Integer update(DetectionInfo detectionInfo);

    Integer delete(int bh);

    Integer insert(DetectionInfo detectionInfo);

}
