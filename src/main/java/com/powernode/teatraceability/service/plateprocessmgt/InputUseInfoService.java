package com.powernode.teatraceability.service.plateprocessmgt;

import com.powernode.teatraceability.pojo.plateprocessmgt.InputUseInfo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface InputUseInfoService {

    List<InputUseInfo> selectLikeByTrpmc(String qydm, String trpmc);

    List<InputUseInfo> selectByQydm(String qydm);

    Integer update(InputUseInfo useInfo);

    Integer insert(InputUseInfo useInfo);

    Integer delete(int bh);


}
