package com.powernode.teatraceability.service.plateprocessmgt.impl;

import com.powernode.teatraceability.dao.basicinfomgt.EnterpriseMapper;
import com.powernode.teatraceability.dao.plateprocessmgt.FarmWorkInfoMapper;
import com.powernode.teatraceability.pojo.plateprocessmgt.FarmWorkInfo;
import com.powernode.teatraceability.service.plateprocessmgt.FarmWorkInfoService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

@Service
@Transactional
public class FarmWorkInfoServiceImpl implements FarmWorkInfoService {

    @Resource
    private FarmWorkInfoMapper farmWorkInfoMapper;

    @Resource
    private EnterpriseMapper enterpriseMapper;

    @Override
    public List<FarmWorkInfo> selectLike(String qydm, String nszy) {
        List<FarmWorkInfo> farmWorkInfos = farmWorkInfoMapper.selectLike(qydm, nszy);
        farmWorkInfos.forEach(farmWorkInfo -> farmWorkInfo.setQymc(enterpriseMapper.selectQYMCByQydm(enterpriseMapper.selectQYMCByQydm(farmWorkInfo.getQydm()))));
        return farmWorkInfos;
    }

    @Transactional(readOnly = true)
    @Override
    public List<FarmWorkInfo> selectAllByQydm(String qydm) {
        List<FarmWorkInfo> farmWorkInfos = farmWorkInfoMapper.selectAllByQydm(qydm);
        final String qymc = enterpriseMapper.selectQYMCByQydm(qydm);
        farmWorkInfos.forEach(farmWorkInfo -> farmWorkInfo.setQymc(qymc));
        return farmWorkInfos;
    }

    @Override
    public List<FarmWorkInfo> selectAllByZyr(String zyr,String qydm) {
        List<FarmWorkInfo> farmWorkInfos = farmWorkInfoMapper.selectAllByZyr(zyr);
        final String qymc = enterpriseMapper.selectQYMCByQydm(qydm);
        farmWorkInfos.forEach(farmWorkInfo -> farmWorkInfo.setQymc(qymc));
        return farmWorkInfos;
    }

    @Override
    public Integer insert(FarmWorkInfo farmWorkInfo) {
        return farmWorkInfoMapper.insert(farmWorkInfo);
    }

    @Override
    public Integer delete(int bh) {
        return farmWorkInfoMapper.delete(bh);
    }

    @Override
    public Integer update(FarmWorkInfo farmWorkInfo) {
        return farmWorkInfoMapper.update(farmWorkInfo);
    }
}
