package com.powernode.teatraceability.service.plateprocessmgt;

import com.powernode.teatraceability.pojo.plateprocessmgt.ProductBatch;
import com.powernode.teatraceability.pojo.sourceinfo.BhInfo;

import java.util.List;

public interface ProductBatchService {

    List<ProductBatch> selectLike(String qydm, String cpmc);

    String queryCpbhByPch(String pch);

    String queryCdbhByPch(String pch);

    List<String> queryAllPch();


    List<ProductBatch> selectAllInfo(String qydm);

    Integer delete(int bh);

    Integer insert(ProductBatch productBatch);

}
