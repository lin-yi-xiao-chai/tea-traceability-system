package com.powernode.teatraceability.service.plateprocessmgt;

import com.powernode.teatraceability.pojo.plateprocessmgt.Input;

public interface InputService {

    Input queryByDjzh(String nydjzh);// 不分页的话用query，就不使用动态代理了

}
