package com.powernode.teatraceability.service.plateprocessmgt.impl;

import com.powernode.teatraceability.dao.basicinfomgt.EnterpriseMapper;
import com.powernode.teatraceability.dao.plateprocessmgt.ProductionEnvironmentMapper;
import com.powernode.teatraceability.pojo.plateprocessmgt.ProductionEnvironment;
import com.powernode.teatraceability.service.plateprocessmgt.ProductionEnvironmentService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;


@Service
public class ProductionEnvironmentServiceImpl implements ProductionEnvironmentService {

    @Resource
    private ProductionEnvironmentMapper productionEnvironmentMapper;
    @Resource
    private EnterpriseMapper enterpriseMapper;

    @Override
    public List<ProductionEnvironment> selectAll(String qydm) {
        List<ProductionEnvironment> productionEnvironments = productionEnvironmentMapper.selectAll();
        final String qymc = enterpriseMapper.selectQYMCByQydm(qydm);
        productionEnvironments.forEach(productionEnvironment -> productionEnvironment.setQymc(qymc));
        return productionEnvironments;
    }
}
