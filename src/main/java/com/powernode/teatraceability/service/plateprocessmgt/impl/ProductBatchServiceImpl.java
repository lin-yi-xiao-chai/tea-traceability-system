package com.powernode.teatraceability.service.plateprocessmgt.impl;

import com.powernode.teatraceability.dao.basicinfomgt.EnterpriseMapper;
import com.powernode.teatraceability.dao.basicinfomgt.ProductMapper;
import com.powernode.teatraceability.dao.plateprocessmgt.ProductBatchMapper;
import com.powernode.teatraceability.pojo.plateprocessmgt.ProductBatch;
import com.powernode.teatraceability.pojo.sourceinfo.BhInfo;
import com.powernode.teatraceability.service.plateprocessmgt.ProductBatchService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.swing.plaf.basic.BasicProgressBarUI;
import java.util.List;

@Service
public class ProductBatchServiceImpl implements ProductBatchService {

    @Resource
    private ProductBatchMapper productBatchMapper;
    @Resource
    private ProductMapper productMapper;
    @Resource
    private EnterpriseMapper enterpriseMapper;

    @Override
    public List<ProductBatch> selectLike(String qydm, String cpmc) {
        List<ProductBatch> productBatches = productBatchMapper.selectLike(qydm, cpmc);
        final String qymc = enterpriseMapper.selectQYMCByQydm(qydm);
        productBatches.forEach(productBatch -> productBatch.setQymc(qymc));
        return productBatches;
    }

    @Override
    public String queryCpbhByPch(String pch) {
        return productBatchMapper.selectCpbhByPch(pch);
    }

    @Override
    public String queryCdbhByPch(String pch) {
        return productBatchMapper.selectCdbhByPch(pch);
    }

    @Override
    public List<String> queryAllPch() {
        return productBatchMapper.selectAllPch();
    }

    @Override
    public List<ProductBatch> selectAllInfo(String qydm) {
        List<ProductBatch> productBatches = productBatchMapper.selectAllInfo(qydm);
        final String qymc = enterpriseMapper.selectQYMCByQydm(qydm);
        productBatches.forEach(productBatch -> {
            String cpgg = productMapper.selectGG(productBatch.getCpmc());
            productBatch.setCpgg(cpgg);
            productBatch.setQymc(qymc);
        });
        return productBatches;
    }

    @Override
    public Integer delete(int bh) {
        return productBatchMapper.delete(bh);
    }

    @Override
    public Integer insert(ProductBatch productBatch) {
        return productBatchMapper.insert(productBatch);
    }
}
