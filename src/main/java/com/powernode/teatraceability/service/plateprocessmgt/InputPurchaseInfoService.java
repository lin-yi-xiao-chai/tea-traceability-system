package com.powernode.teatraceability.service.plateprocessmgt;

import com.powernode.teatraceability.pojo.page.PageRequest;
import com.powernode.teatraceability.pojo.page.PageResult;
import com.powernode.teatraceability.pojo.plateprocessmgt.InputPurchaseInfo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface InputPurchaseInfoService {

    List<InputPurchaseInfo> selectLike(String qydm,  String trpmc);

    List<InputPurchaseInfo> selectAllByQydm(String qydm);

    Integer update(InputPurchaseInfo inputPurchaseInfo);

    Integer delete(int bh);

    Integer insert(InputPurchaseInfo inputPurchaseInfo);

}
