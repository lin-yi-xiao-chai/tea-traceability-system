package com.powernode.teatraceability.service.plateprocessmgt;

import com.powernode.teatraceability.pojo.plateprocessmgt.ProductionEnvironment;

import java.util.List;

public interface ProductionEnvironmentService {

    List<ProductionEnvironment> selectAll(String qydm);

}
