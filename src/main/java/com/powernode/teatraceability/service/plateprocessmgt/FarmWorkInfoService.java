package com.powernode.teatraceability.service.plateprocessmgt;

import com.powernode.teatraceability.pojo.plateprocessmgt.FarmWorkInfo;

import java.util.List;

public interface FarmWorkInfoService {
    List<FarmWorkInfo> selectLike(String qydm,String nszy);

    List<FarmWorkInfo> selectAllByQydm(String qydm);

    List<FarmWorkInfo> selectAllByZyr(String zyr,String qydm);

    Integer insert(FarmWorkInfo farmWorkInfo);

    Integer delete(int bh);

    Integer update(FarmWorkInfo farmWorkInfo);

}
