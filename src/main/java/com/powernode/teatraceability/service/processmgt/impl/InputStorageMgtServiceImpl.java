package com.powernode.teatraceability.service.processmgt.impl;

import com.powernode.teatraceability.dao.basicinfomgt.EnterpriseMapper;
import com.powernode.teatraceability.dao.processmgt.InputStorageMgtMapper;
import com.powernode.teatraceability.dao.processmgt.StorageMgtMapper;
import com.powernode.teatraceability.pojo.processmgt.InputStorageMgt;
import com.powernode.teatraceability.service.processmgt.InputStorageMgtService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

@Service
@Transactional
public class InputStorageMgtServiceImpl implements InputStorageMgtService {

    @Resource
    private InputStorageMgtMapper inputStorageMgtMapper;
    @Resource
    private StorageMgtMapper storageMgtMapper;

    @Override
    public List<InputStorageMgt> selectLike(String qydm, String supplyName) {
        return inputStorageMgtMapper.selectLike(qydm, supplyName);
    }

    @Override
    public List<InputStorageMgt> selectAll(String qydm) {
        return inputStorageMgtMapper.selectAll(qydm);
    }

    @Override
    public Integer insert(InputStorageMgt inputStorageMgt) {
        inputStorageMgt.setStorageId(queryIdByName(inputStorageMgt.getStorageName()));
        return inputStorageMgtMapper.insert(inputStorageMgt);
    }

    @Override
    public Integer update(InputStorageMgt inputStorageMgt) {
        inputStorageMgt.setStorageId(queryIdByName(inputStorageMgt.getStorageName()));
        return inputStorageMgtMapper.update(inputStorageMgt);
    }

    private Integer queryIdByName(String name){
        return storageMgtMapper.selectIdByName(name);
    }

    @Override
    public Integer delete(int id) {
        return inputStorageMgtMapper.delete(id);
    }
}
