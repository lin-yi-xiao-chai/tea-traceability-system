package com.powernode.teatraceability.service.processmgt.impl;

import com.powernode.teatraceability.dao.processmgt.OutputStorageMgtMapper;
import com.powernode.teatraceability.dao.processmgt.StorageMgtMapper;
import com.powernode.teatraceability.pojo.processmgt.InputStorageMgt;
import com.powernode.teatraceability.pojo.processmgt.OutputStorageMgt;
import com.powernode.teatraceability.service.processmgt.OutputStorageMgtService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

@Service
@Transactional
public class OutputStorageMgtServiceImpl implements OutputStorageMgtService {

    @Resource
    private OutputStorageMgtMapper outputStorageMgtMapper;

    @Resource
    private StorageMgtMapper storageMgtMapper;

    @Override
    public List<InputStorageMgt> selectLike(String qydm, String cpmc) {
        return outputStorageMgtMapper.selectLike(qydm, cpmc);
    }

    @Transactional(readOnly = true)
    @Override
    public List<InputStorageMgt> selectAll(String qydm) {
        return outputStorageMgtMapper.selectAll(qydm);
    }

    @Override
    public Integer insert(OutputStorageMgt outputStorageMgt) {
        outputStorageMgt.setStorageId(queryIdByName(outputStorageMgt.getStorageName()));
        return outputStorageMgtMapper.insert(outputStorageMgt);
    }

    @Override
    public Integer update(OutputStorageMgt outputStorageMgt) {
        outputStorageMgt.setStorageId(queryIdByName(outputStorageMgt.getStorageName()));
        return outputStorageMgtMapper.update(outputStorageMgt);
    }

    private Integer queryIdByName(String name){
        return storageMgtMapper.selectIdByName(name);
    }

    @Override
    public Integer delete(int id) {
        return outputStorageMgtMapper.delete(id);
    }
}
