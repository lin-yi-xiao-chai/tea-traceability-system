package com.powernode.teatraceability.service.processmgt.impl;

import com.powernode.teatraceability.dao.basicinfomgt.EnterpriseMapper;
import com.powernode.teatraceability.dao.processmgt.StorageMgtMapper;
import com.powernode.teatraceability.pojo.processmgt.StorageMgt;
import com.powernode.teatraceability.service.processmgt.StorageMgtService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

@Service
@Transactional
public class StorageMgtServiceImpl implements StorageMgtService {

    @Resource
    private StorageMgtMapper storageMgtMapper;
    @Override
    public List<StorageMgt> selectLike(String storageName) {
        return storageMgtMapper.selectLike(storageName);
    }

    @Override
    public List<String> queryAllStorage() {
        return storageMgtMapper.selectAllStorage();
    }

    @Transactional(readOnly = true)
    @Override
    public List<StorageMgt> selectAll() {
        return storageMgtMapper.selectAll();
    }

    @Override
    public Integer update(StorageMgt storageMgt) {
        return storageMgtMapper.update(storageMgt);
    }

    @Override
    public Integer delete(int id) {
        return storageMgtMapper.delete(id);
    }

    @Override
    public Integer insert(StorageMgt storageMgt) {
        return storageMgtMapper.insert(storageMgt);
    }
}
