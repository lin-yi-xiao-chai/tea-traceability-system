package com.powernode.teatraceability.service.processmgt;

import com.powernode.teatraceability.pojo.processmgt.InputStorageMgt;

import java.util.List;

public interface InputStorageMgtService {

    List<InputStorageMgt> selectLike(String qydm,String supplyName);

    List<InputStorageMgt> selectAll(String qydm);

    Integer insert(InputStorageMgt inputStorageMgt);

    Integer update(InputStorageMgt inputStorageMgt);

    Integer delete(int id);

}
