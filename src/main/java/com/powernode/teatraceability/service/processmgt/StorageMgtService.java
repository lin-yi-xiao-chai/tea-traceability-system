package com.powernode.teatraceability.service.processmgt;

import com.powernode.teatraceability.pojo.processmgt.StorageMgt;

import java.util.List;

public interface StorageMgtService {

    List<StorageMgt> selectLike(String storageName);

    List<String> queryAllStorage();

    List<StorageMgt> selectAll();

    Integer update(StorageMgt storageMgt);

    Integer delete(int id);

    Integer insert(StorageMgt storageMgt);

}
