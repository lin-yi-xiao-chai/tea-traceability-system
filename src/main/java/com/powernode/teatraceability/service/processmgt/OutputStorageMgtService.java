package com.powernode.teatraceability.service.processmgt;

import com.powernode.teatraceability.pojo.processmgt.InputStorageMgt;
import com.powernode.teatraceability.pojo.processmgt.OutputStorageMgt;

import java.util.List;

public interface OutputStorageMgtService {

    List<InputStorageMgt> selectLike(String qydm,String cpmc);

    List<InputStorageMgt> selectAll(String qydm);

    Integer insert(OutputStorageMgt outputStorageMgt);

    Integer update(OutputStorageMgt outputStorageMgt);

    Integer delete(int id);

}
