package com.powernode.teatraceability.service.processmgt;

import com.powernode.teatraceability.pojo.processmgt.ProcessMgt;

import java.util.List;

public interface ProcessMgtService {
    List<ProcessMgt> selectLike(String qydm,String fzr);

    Integer deleteById(int id);

    Integer update(ProcessMgt processMgt);

    Integer insert(ProcessMgt processMgt);

    List<ProcessMgt> selectAll(String qydm);

}
