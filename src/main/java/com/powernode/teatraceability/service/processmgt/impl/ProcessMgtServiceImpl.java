package com.powernode.teatraceability.service.processmgt.impl;

import com.powernode.teatraceability.dao.basicinfomgt.EnterpriseMapper;
import com.powernode.teatraceability.dao.processmgt.ProcessMgtMapper;
import com.powernode.teatraceability.pojo.processmgt.ProcessMgt;
import com.powernode.teatraceability.service.processmgt.ProcessMgtService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

@Service
@Transactional
public class ProcessMgtServiceImpl implements ProcessMgtService {

    @Resource
    private ProcessMgtMapper processMgtMapper;

    @Resource
    private EnterpriseMapper enterpriseMapper;

    @Override
    public List<ProcessMgt> selectLike(String qydm, String fzr) {
        List<ProcessMgt> processMgts = processMgtMapper.selectLike(qydm, fzr);
        final String qymc = enterpriseMapper.selectQYMCByQydm(qydm);
        processMgts.forEach(processMgt -> processMgt.setQymc(qymc));
        return processMgts;
    }

    @Override
    public Integer deleteById(int id) {
        return processMgtMapper.deleteById(id);
    }

    @Override
    public Integer update(ProcessMgt processMgt) {
        return processMgtMapper.update(processMgt);
    }

    @Override
    public Integer insert(ProcessMgt processMgt) {
        return processMgtMapper.insert(processMgt);
    }

    @Override
    @Transactional(readOnly = true)
    public List<ProcessMgt> selectAll(String qydm) {
        List<ProcessMgt> processMgts = processMgtMapper.selectAll(qydm);
        final String qymc = enterpriseMapper.selectQYMCByQydm(qydm);
        processMgts.forEach(processMgt -> processMgt.setQymc(qymc));
        return processMgts;
    }
}
