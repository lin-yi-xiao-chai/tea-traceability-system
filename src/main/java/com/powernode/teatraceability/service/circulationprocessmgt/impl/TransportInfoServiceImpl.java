package com.powernode.teatraceability.service.circulationprocessmgt.impl;

import com.powernode.teatraceability.dao.circulationprocessmgt.ClientInfoMapper;
import com.powernode.teatraceability.dao.circulationprocessmgt.TransportInfoMapper;
import com.powernode.teatraceability.dao.plateprocessmgt.ProductBatchMapper;
import com.powernode.teatraceability.pojo.circulationprocessmgt.TransportInfo;
import com.powernode.teatraceability.pojo.plateprocessmgt.ProductBatch;
import com.powernode.teatraceability.service.circulationprocessmgt.TransportInfoService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;


@Service
@Transactional
public class TransportInfoServiceImpl implements TransportInfoService {

    @Resource
    private TransportInfoMapper transportInfoMapper;

    @Resource
    private ClientInfoMapper clientInfoMapper;

    @Resource
    private ProductBatchMapper productBatchMapper;

    @Override
    public List<TransportInfo> selectLike(String qydm, String shiperName) {
        List<TransportInfo> transportInfos = transportInfoMapper.selectLike(qydm, shiperName);
        transportInfos.forEach(transportInfo -> {
            transportInfo.setCpmc(productBatchMapper.selectCpmcByPch(transportInfo.getPch()));
        });
        return transportInfos;
    }

    @Override
    public List<TransportInfo> selectAll(String qydm) {
        List<TransportInfo> transportInfos = transportInfoMapper.selectAll(qydm);
        transportInfos.forEach(transportInfo -> {
            transportInfo.setCpmc(productBatchMapper.selectCpmcByPch(transportInfo.getPch()));
        });
        return transportInfos;
    }

    @Override
    public Integer update(TransportInfo transportInfo) {
        return transportInfoMapper.update(transportInfo);
    }

    @Override
    public Integer insert(TransportInfo transportInfo) {
        transportInfo.setClientId(clientInfoMapper.selectByClientName(transportInfo.getClientName()));
        return transportInfoMapper.insert(transportInfo);
    }
}
