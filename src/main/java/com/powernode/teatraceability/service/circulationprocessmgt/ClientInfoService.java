package com.powernode.teatraceability.service.circulationprocessmgt;

import com.powernode.teatraceability.pojo.circulationprocessmgt.ClientInfo;

import java.util.List;

public interface ClientInfoService {

    List<ClientInfo> selectLike(String qydm,String clientName);

    List<String> queryAllClient(String qydm);

    List<ClientInfo> selectAll(String qydm);

    Integer insert(ClientInfo clientInfo);

    Integer update(ClientInfo clientInfo);

    Integer delete(int id);

}
