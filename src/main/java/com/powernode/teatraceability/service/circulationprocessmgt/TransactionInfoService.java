package com.powernode.teatraceability.service.circulationprocessmgt;

import com.powernode.teatraceability.pojo.circulationprocessmgt.TransactionInfo;

import java.util.List;

public interface TransactionInfoService {

    List<TransactionInfo> selectLike(String qydm,String name);

    List<TransactionInfo> selectAll(String qydm);

    Integer update(TransactionInfo transactionInfo);

    Integer insert(TransactionInfo transactionInfo);

    Integer delete(int id);

}
