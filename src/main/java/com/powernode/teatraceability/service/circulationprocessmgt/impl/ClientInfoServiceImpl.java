package com.powernode.teatraceability.service.circulationprocessmgt.impl;

import com.powernode.teatraceability.dao.circulationprocessmgt.ClientInfoMapper;
import com.powernode.teatraceability.pojo.circulationprocessmgt.ClientInfo;
import com.powernode.teatraceability.service.circulationprocessmgt.ClientInfoService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;


@Service
@Transactional
public class ClientInfoServiceImpl implements ClientInfoService {

    @Resource
    private ClientInfoMapper clientInfoMapper;

    @Override
    public List<ClientInfo> selectLike(String qydm, String clientName) {
        return clientInfoMapper.selectLike(qydm, clientName);
    }

    @Override
    public List<String> queryAllClient(String qydm) {
        return clientInfoMapper.selectAllClient(qydm);
    }

    @Override
    public List<ClientInfo> selectAll(String qydm) {
        return clientInfoMapper.selectAll(qydm);
    }

    @Override
    public Integer insert(ClientInfo clientInfo) {
        return clientInfoMapper.insert(clientInfo);
    }

    @Override
    public Integer update(ClientInfo clientInfo) {
        return clientInfoMapper.update(clientInfo);
    }

    @Override
    public Integer delete(int id) {
        return clientInfoMapper.delete(id);
    }
}
