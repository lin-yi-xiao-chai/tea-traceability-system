package com.powernode.teatraceability.service.circulationprocessmgt;

import com.powernode.teatraceability.pojo.circulationprocessmgt.TransportInfo;

import java.util.List;

public interface TransportInfoService {

    List<TransportInfo> selectLike(String qydm,String shiperName);

    List<TransportInfo> selectAll(String qydm);

    Integer update(TransportInfo transportInfo);

    Integer insert(TransportInfo transportInfo);

}
