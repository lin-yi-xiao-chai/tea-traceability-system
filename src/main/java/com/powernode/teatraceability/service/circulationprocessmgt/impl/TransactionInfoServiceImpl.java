package com.powernode.teatraceability.service.circulationprocessmgt.impl;

import com.powernode.teatraceability.dao.circulationprocessmgt.TransactionInfoMapper;
import com.powernode.teatraceability.pojo.circulationprocessmgt.TransactionInfo;
import com.powernode.teatraceability.service.circulationprocessmgt.TransactionInfoService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;


@Service
@Transactional
public class TransactionInfoServiceImpl implements TransactionInfoService {

    @Resource
    private TransactionInfoMapper transactionInfoMapper;

    @Override
    public List<TransactionInfo> selectLike(String qydm, String name) {
        return transactionInfoMapper.selectLike(qydm, name);
    }

    @Override
    public List<TransactionInfo> selectAll(String qydm) {
        return transactionInfoMapper.selectAll(qydm);
    }

    @Override
    public Integer update(TransactionInfo transactionInfo) {
        return transactionInfoMapper.update(transactionInfo);
    }

    @Override
    public Integer insert(TransactionInfo transactionInfo) {
        return transactionInfoMapper.insert(transactionInfo);
    }

    @Override
    public Integer delete(int id) {
        return transactionInfoMapper.delete(id);
    }
}
