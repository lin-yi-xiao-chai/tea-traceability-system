package com.powernode.teatraceability.service.user.impl;

import com.powernode.teatraceability.dao.login.UserMapper;
import com.powernode.teatraceability.pojo.login.User;
import com.powernode.teatraceability.service.user.UserService;
import com.powernode.teatraceability.util.GlobalConfig;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
public class UserServiceImpl implements UserService {

    @Resource
    private UserMapper userMapper;

    @Override
    public boolean verify(String username, String password, String groupName) {

        User user = userMapper.selectByUserId(username);
        if(username.equals(user.getRybh()) &&
                password.equals(user.getKl()) &&
                groupName.equals(user.getGroupname())){
            GlobalConfig.setUser(user);
            return true;
        }
        return false;
    }
}
