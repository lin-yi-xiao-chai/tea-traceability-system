package com.powernode.teatraceability.service.user;

public interface UserService {

    /**
     * 人员身份验证，判断是否符合
     * @param username 人员编号
     * @param password 人员密码（口令）
     * @param groupName 组织名称
     * @return true则符合，反之false
     */
    boolean verify(String username, String password, String groupName);

}
