package com.powernode.teatraceability.service.basicinfomgt;


import com.powernode.teatraceability.pojo.basicinfomgt.Enterprise;

public interface EnterpriseService {

    /**
     * 收集企业信息
     */
    Enterprise queryByQydm(String qydm);

    /**
     * 更新数据库企业信息
     */
    Integer updateEnterprise(Enterprise enterprise,String qydm);

    String selectQYMCByQydm(String qydm);

}
