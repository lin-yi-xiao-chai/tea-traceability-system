package com.powernode.teatraceability.service.basicinfomgt.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.powernode.teatraceability.dao.basicinfomgt.OriginMapper;
import com.powernode.teatraceability.pojo.basicinfomgt.Origin;
import com.powernode.teatraceability.pojo.page.PageResult;
import com.powernode.teatraceability.service.basicinfomgt.EnterpriseService;
import com.powernode.teatraceability.service.basicinfomgt.OriginService;
import com.powernode.teatraceability.util.JsonData;
import com.powernode.teatraceability.util.PageUtil;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

@Service
@Transactional
public class OriginServiceImpl implements OriginService {
    @Resource
    private OriginMapper originMapper;

    @Resource
    private EnterpriseService enterpriseService;

    /**
     * 查询所有的产地名称
     * @param qydm
     * @return
     */
   public List queryAllChandi(String qydm){
        return originMapper.selectAllChandi(qydm);
    }

    @Override
    public String queryCdbhByCdmc(String cdmc) {
        return originMapper.selectCdbhByCdmc(cdmc);
    }

    /**
     * 模糊查询
     * @param chandi
     * @param qydm
     * @return
     */
    @Override
    @Transactional(readOnly = true)
    public List selectByChanDi(String chandi, String qydm) {
        List<Origin> origins = originMapper.likeSelectByChanDi(chandi, qydm);
        addQymc(origins,qydm);
        return origins;
    }

    /**
     * 查询总的
     */
    @Override
    @Transactional(readOnly = true)
    public List selectByQydm(String qydm) {
        List<Origin> origins = originMapper.selectByQydm(qydm);
        addQymc(origins,qydm);
        return origins;
    }


    /**
     * 编辑数据
     * @param origin
     */
    @Override
    public Integer update(Origin origin) {
        return originMapper.update(origin);
    }

    /**
     * 插入数据
     * @param origin
     */
    @Override
    public Integer insert(Origin origin) {
        return originMapper.insert(origin);
    }

    /**
     * 删除数据
     * @param bh
     */
    @Override
    public Integer deleteByBh(String bh) {
        return originMapper.deleteByBh(bh);
    }
    private JsonData<PageResult> success(PageResult result){
        JsonData<PageResult> jsonData = new JsonData<>();
        jsonData.setData(result);
        jsonData.setStatus(200);
        jsonData.setMsg("ok");
        return jsonData;
    }
    private void addQymc(List<Origin> origins,String qydm){
        final String qymc = enterpriseService.selectQYMCByQydm(qydm);
        origins.stream().forEach(origin->origin.setQymc(qymc));
    }
}
