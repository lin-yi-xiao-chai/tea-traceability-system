package com.powernode.teatraceability.service.basicinfomgt;

import com.powernode.teatraceability.pojo.basicinfomgt.Origin;
import com.powernode.teatraceability.pojo.page.PageResult;
import com.powernode.teatraceability.util.JsonData;

import java.util.List;

public interface OriginService {

    String queryCdbhByCdmc(String cdmc);

    List selectByChanDi(String chandi, String qydm);

    List selectByQydm(String qydm);

    List queryAllChandi(String qydm);

    Integer update(Origin origin);

    Integer insert(Origin origin);

    Integer deleteByBh(String bh);

}
