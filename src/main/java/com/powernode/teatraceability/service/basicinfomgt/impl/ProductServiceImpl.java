package com.powernode.teatraceability.service.basicinfomgt.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.powernode.teatraceability.dao.basicinfomgt.EnterpriseMapper;
import com.powernode.teatraceability.dao.basicinfomgt.ProductMapper;
import com.powernode.teatraceability.pojo.page.PageResult;
import com.powernode.teatraceability.pojo.basicinfomgt.Product;
import com.powernode.teatraceability.service.basicinfomgt.EnterpriseService;
import com.powernode.teatraceability.service.basicinfomgt.ProductService;
import com.powernode.teatraceability.util.JsonData;
import com.powernode.teatraceability.util.PageUtil;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

@Service
@Transactional
public class ProductServiceImpl implements ProductService {

    @Resource
    private ProductMapper productMapper;
    @Resource
    private EnterpriseMapper enterpriseMapper;

    @Override
    public String queryCpbhByCpmc(String cpmc) {
        return productMapper.selectCpbhByCpmc(cpmc);
    }

    @Override
    public String getOneCpbh() {
        List<String> cpbhs = productMapper.selectAllCpbh();
        while(true){
            final String ans = String.valueOf((int) (Math.random() * 100000));
            for (String cpbh : cpbhs) {
                if (ans.equals(cpbh)) {
                    continue;
                }else{
                    return ans;
                }
            }
        }
    }

    @Override
    @Transactional(readOnly = true)
    public List<String> queryAllProduct(){
        return productMapper.selectAllProduct();
    }

    @Override
    @Transactional(readOnly = true)
    public List selectAllByQydm(String qydm) {
        List<Product> products = productMapper.selectAllByQydm(qydm);
        addQymc(products, qydm);
        return products;
    }

    @Override
    @Transactional(readOnly = true)
    public List selectLikeByChanpinName(String cpmc, String qydm) {
        List<Product> products = productMapper.likeSelectByChanpinName(cpmc, qydm);
        addQymc(products,qydm);
        return products;
    }

    private JsonData<PageResult> success(PageResult result){
        JsonData<PageResult> jsonData = new JsonData<>();
        jsonData.setData(result);
        jsonData.setStatus(200);
        jsonData.setMsg("ok");
        return jsonData;
    }

    @Override
    public Integer deleteByBh(int bh) {
        return productMapper.deleteByBh(bh);
    }

    @Override
    public Integer update(Product product) {
        return productMapper.update(product);
    }

    @Override
    public Integer insert(Product product) {
        return productMapper.insert(product);
    }

    /**
     * 给企业名称属性初始化
     * @param products
     * @param qydm
     */
    private void addQymc(List<Product> products, String qydm){
        final String qymc = enterpriseMapper.selectQYMCByQydm(qydm);
        products.stream().forEach(product->product.setQymc(qymc));
    }
}
