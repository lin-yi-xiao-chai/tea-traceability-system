package com.powernode.teatraceability.service.basicinfomgt;

import com.powernode.teatraceability.pojo.basicinfomgt.Product;
import com.powernode.teatraceability.util.JsonData;

import java.util.List;

public interface ProductService {

    String queryCpbhByCpmc(String cpmc);

    String getOneCpbh();

    List<String> queryAllProduct();

    List selectAllByQydm(String qydm);

    List selectLikeByChanpinName(String cpmc, String qydm);

    Integer deleteByBh(int bh);

    Integer update(Product product);


    Integer insert(Product product);
}
