package com.powernode.teatraceability.service.basicinfomgt.impl;

import com.powernode.teatraceability.dao.basicinfomgt.EnterpriseMapper;
import com.powernode.teatraceability.pojo.basicinfomgt.Enterprise;
import com.powernode.teatraceability.service.basicinfomgt.EnterpriseService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

@Service
@Transactional
public class EnterpriseServiceImpl implements EnterpriseService {

    @Resource
    private EnterpriseMapper enterpriseMapper;



    @Override
    public Enterprise queryByQydm(String qydm) {
        return enterpriseMapper.selectOneByQydm(qydm);
    }

    @Override
    public Integer updateEnterprise(Enterprise enterprise, String qydm) {
        return enterpriseMapper.update(enterprise);
    }

    @Override
    public String selectQYMCByQydm(String qydm) {
        return enterpriseMapper.selectQYMCByQydm(qydm);
    }
}
